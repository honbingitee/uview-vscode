/*
 * @Author: hongbin
 * @Date: 2023-09-15 17:21:15
 * @LastEditors: hongbin
 * @LastEditTime: 2023-09-18 09:23:49
 * @Description:u-view文档
 */
import * as vscode from 'vscode';
// const reg = new RegExp(/^@property (\{.+\}) ([\w|\-]+) (.+)/);
const prop_reg = new RegExp(/^(@property) (\{.+\}) ([\w|\-]+) (.+)/);
const event_reg = new RegExp(/^(@event) (\{.+\}) ([\w|\-]+) (.+)/);

type IPropOrMethod = Record<string, { desc: string; item: vscode.CompletionItem }>;

/**
 * 将u-view文档上的组件注释转成描述
 */
const explanatoryNoteToDesc = (str: string) => {
	const arr = str.split('\n');
	arr.pop();
	arr.shift();
	const desc = arr.map((s) => s.substring(4));
	const properties: IPropOrMethod = {};
	const method: IPropOrMethod = {};

	desc.forEach((text) => {
		const matchArray = text.match(prop_reg) || text.match(event_reg);
		// console.log(matchArray);
		if (matchArray && matchArray[3]) {
			const isProp = matchArray[1] === '@property';
			const itemType = vscode.CompletionItemKind[isProp ? 'Field' : 'Method'];
			const res = {
				item: new vscode.CompletionItem(matchArray[3], itemType),
				desc: matchArray[4],
			};
			if (isProp) properties[matchArray[3]] = res;
			else method[matchArray[3]] = res;
		}
	});

	return {
		desc,
		propertiesAndMethod: { ...properties, ...method },
		properties,
		method,
	};
};

const _docs = {
	'u-switch': `/**
	 * switch 开关选择器
	 * @description 选择开关一般用于只有两个选择，且只能选其一的场景。
	 * @tutorial https://www.uviewui.com/components/switch.html
	 * @property {Boolean} loading 是否处于加载中（默认false）
	 * @property {Boolean} disabled 是否禁用（默认false）
	 * @property {String Number} size 开关尺寸，单位rpx（默认50）
	 * @property {String} active-color 打开时的背景色（默认#2979ff）
	 * @property {Boolean} inactive-color 关闭时的背景色（默认#ffffff）
	 * @property {Boolean | Number | String} active-value 打开选择器时通过change事件发出的值（默认true）
	 * @property {Boolean | Number | String} inactive-value 关闭选择器时通过change事件发出的值（默认false）
	 * @event {Function} change 在switch打开或关闭时触发
	 * @example \\<u-switch v-model="checked" active-color="red" inactive-color="#eee">\\</u-switch>
	 */`,
	'u-action-sheet': `/**
	 * actionSheet 操作菜单
	 * @description 本组件用于从底部弹出一个操作菜单，供用户选择并返回结果。本组件功能类似于uni的uni.showActionSheetAPI，配置更加灵活，所有平台都表现一致。
	 * @tutorial https://www.uviewui.com/components/actionSheet.html
	 * @property {Array<Object>} list 按钮的文字数组，见官方文档示例
	 * @property {Object} tips 顶部的提示文字，见官方文档示例
	 * @property {String} cancel-text 取消按钮的提示文字
	 * @property {Boolean} cancel-btn 是否显示底部的取消按钮（默认true）
	 * @property {Number String} border-radius 弹出部分顶部左右的圆角值，单位rpx（默认0）
	 * @property {Boolean} mask-close-able 点击遮罩是否可以关闭（默认true）
	 * @property {Boolean} safe-area-inset-bottom 是否开启底部安全区适配（默认false）
	 * @property {Number String} z-index z-index值（默认1075）
	 * @property {String} cancel-text 取消按钮的提示文字
	 * @event {Function} click 点击ActionSheet列表项时触发
	 * @event {Function} close 点击取消按钮时触发
	 * @example \\<u-action-sheet :list="list" @click="click" v-model="show">\\</u-action-sheet>
	 */`,
	'u-alert-tips': `/**
	 * alertTips 警告提示
	 * @description 警告提示，展现需要关注的信息
	 * @tutorial https://uviewui.com/components/alertTips.html
	 * @property {String} title 显示的标题文字
	 * @property {String} description 辅助性文字，颜色比title浅一点，字号也小一点，可选
	 * @property {String} type 关闭按钮(默认为叉号icon图标)
	 * @property {String} icon 图标名称
	 * @property {Object} icon-style 图标的样式，对象形式
	 * @property {Object} title-style 标题的样式，对象形式
	 * @property {Object} desc-style 描述的样式，对象形式
	 * @property {String} close-able 用文字替代关闭图标，close-able为true时有效
	 * @property {Boolean} show-icon 是否显示左边的辅助图标
	 * @property {Boolean} show 显示或隐藏组件
	 * @event {Function} click 点击组件时触发
	 * @event {Function} close 点击关闭按钮时触发
	 */
	 `,
	'u-avatar': `/**
	 * avatar 头像
	 * @description 本组件一般用于展示头像的地方，如个人中心，或者评论列表页的用户头像展示等场所。
	 * @tutorial https://www.uviewui.com/components/avatar.html
	 * @property {String} bg-color 背景颜色，一般显示文字时用（默认#ffffff）
	 * @property {String} src 头像路径，如加载失败，将会显示默认头像
	 * @property {String Number} size 头像尺寸，可以为指定字符串(large, default, mini)，或者数值，单位rpx（默认default）
	 * @property {String} mode 显示类型，见上方说明（默认circle）
	 * @property {String} sex-icon 性别图标，man-男，woman-女（默认man）
	 * @property {String} level-icon 等级图标（默认level）
	 * @property {String} sex-bg-color 性别图标背景颜色
	 * @property {String} level-bg-color 等级图标背景颜色
	 * @property {String} show-sex 是否显示性别图标（默认false）
	 * @property {String} show-level 是否显示等级图标（默认false）
	 * @property {String} img-mode 头像图片的裁剪类型，与uni的image组件的mode参数一致，如效果达不到需求，可尝试传widthFix值（默认aspectFill）
	 * @property {String} index 用户传递的标识符值，如果是列表循环，可穿v-for的index值
	 * @event {Function} click 头像被点击
	 * @example \\<u-avatar :src="src">\\</u-avatar>
	 */`,
	'u-avatar-cropper': `/**
	 * @description we-cropper v1.3.9
	 */`,
	'u-back-top': ``,
	'u-badge': `/**
	 * badge 角标
	 * @description 本组件一般用于展示头像的地方，如个人中心，或者评论列表页的用户头像展示等场所。
	 * @tutorial https://www.uviewui.com/components/badge.html
	 * @property {String Number} count 展示的数字，大于 overflowCount 时显示为 overflowCount+，为0且show-zero为false时隐藏
	 * @property {Boolean} is-dot 不展示数字，只有一个小点（默认false）
	 * @property {Boolean} absolute 组件是否绝对定位，为true时，offset参数才有效（默认true）
	 * @property {String Number} overflow-count 展示封顶的数字值（默认99）
	 * @property {String} type 使用预设的背景颜色（默认error）
	 * @property {Boolean} show-zero 当数值为 0 时，是否展示 Badge（默认false）
	 * @property {String} size Badge的尺寸，设为mini会得到小一号的Badge（默认default）
	 * @property {Array} offset 设置badge的位置偏移，格式为 [x, y]，也即设置的为top和right的值，单位rpx。absolute为true时有效（默认[20, 20]）
	 * @property {String} color 字体颜色（默认#ffffff）
	 * @property {String} bgColor 背景颜色，优先级比type高，如设置，type参数会失效
	 * @property {Boolean} is-center 组件中心点是否和父组件右上角重合，优先级比offset高，如设置，offset参数会失效（默认false）
	 * @example \\<u-badge type="error" count="7">\\</u-badge>
	 */`,
	'u-button': `/**
	 * button 按钮
	 * @description Button 按钮
	 * @tutorial https://www.uviewui.com/components/button.html
	 * @property {String} size 按钮的大小
	 * @property {Boolean} ripple 是否开启点击水波纹效果
	 * @property {String} ripple-bg-color 水波纹的背景色，ripple为true时有效
	 * @property {String} type 按钮的样式类型
	 * @property {Boolean} plain 按钮是否镂空，背景色透明
	 * @property {Boolean} disabled 是否禁用
	 * @property {Boolean} hair-line 是否显示按钮的细边框(默认true)
	 * @property {Boolean} shape 按钮外观形状，见文档说明
	 * @property {Boolean} loading 按钮名称前是否带 loading 图标(App-nvue 平台，在 ios 上为雪花，Android上为圆圈)
	 * @property {String} form-type 用于 <form> 组件，点击分别会触发 <form> 组件的 submit/reset 事件
	 * @property {String} open-type 开放能力
	 * @property {String} data-name 额外传参参数，用于小程序的data-xxx属性，通过target.dataset.name获取
	 * @property {String} hover-class 指定按钮按下去的样式类。当 hover-class="none" 时，没有点击态效果(App-nvue 平台暂不支持)
	 * @property {Number} hover-start-time 按住后多久出现点击态，单位毫秒
	 * @property {Number} hover-stay-time 手指松开后点击态保留时间，单位毫秒
	 * @property {Object} custom-style 对按钮的自定义样式，对象形式，见文档说明
	 * @event {Function} click 按钮点击
	 * @event {Function} getphonenumber open-type="getPhoneNumber"时有效
	 * @event {Function} getuserinfo 用户点击该按钮时，会返回获取到的用户信息，从返回参数的detail中获取到的值同uni.getUserInfo
	 * @event {Function} error 当使用开放能力时，发生错误的回调
	 * @event {Function} opensetting 在打开授权设置页并关闭后回调
	 * @event {Function} launchapp 打开 APP 成功的回调
	 * @example \\<u-button>月落\\</u-button>
	 */`,
	'u-calendar': `/**
	 * calendar 日历
	 * @description 此组件用于单个选择日期，范围选择日期等，日历被包裹在底部弹起的容器中。
	 * @tutorial http://uviewui.com/components/calendar.html
	 * @property {String} mode 选择日期的模式，date-为单个日期，range-为选择日期范围
	 * @property {Boolean} v-model 布尔值变量，用于控制日历的弹出与收起
	 * @property {Boolean} safe-area-inset-bottom 是否开启底部安全区适配(默认false)
	 * @property {Boolean} change-year 是否显示顶部的切换年份方向的按钮(默认true)
	 * @property {Boolean} change-month 是否显示顶部的切换月份方向的按钮(默认true)
	 * @property {String Number} max-year 可切换的最大年份(默认2050)
	 * @property {String Number} min-year 最小可选日期(默认1950)
	 * @property {String Number} min-date 可切换的最小年份(默认1950-01-01)
	 * @property {String Number} max-date 最大可选日期(默认当前日期)
	 * @property {String Number} 弹窗顶部左右两边的圆角值，单位rpx(默认20)
	 * @property {Boolean} mask-close-able 是否允许通过点击遮罩关闭日历(默认true)
	 * @property {String} month-arrow-color 月份切换按钮箭头颜色(默认#606266)
	 * @property {String} year-arrow-color 年份切换按钮箭头颜色(默认#909399)
	 * @property {String} color 日期字体的默认颜色(默认#303133)
	 * @property {String} active-bg-color 起始/结束日期按钮的背景色(默认#2979ff)
	 * @property {String Number} z-index 弹出时的z-index值(默认10075)
	 * @property {String} active-color 起始/结束日期按钮的字体颜色(默认#ffffff)
	 * @property {String} range-bg-color 起始/结束日期之间的区域的背景颜色(默认rgba(41,121,255,0.13))
	 * @property {String} range-color 选择范围内字体颜色(默认#2979ff)
	 * @property {String} start-text 起始日期底部的提示文字(默认 '开始')
	 * @property {String} end-text 结束日期底部的提示文字(默认 '结束')
	 * @property {String} btn-type 底部确定按钮的主题(默认 'primary')
	 * @property {String} toolTip 顶部提示文字，如设置名为tooltip的slot，此参数将失效(默认 '选择日期')
	 * @property {Boolean} closeable 是否显示右上角的关闭图标(默认true)
	 * @example \\<u-calendar v-model="show" :mode="mode">\\</u-calendar>
	 */`,
	'u-car-keyboard': ``,
	'u-card': `/**
	 * card 卡片
	 * @description 卡片组件一般用于多个列表条目，且风格统一的场景
	 * @tutorial https://www.uviewui.com/components/card.html
	 * @property {Boolean} full 卡片与屏幕两侧是否留空隙（默认false）
	 * @property {String} title 头部左边的标题
	 * @property {String} title-color 标题颜色（默认#303133）
	 * @property {String | Number} title-size 标题字体大小，单位rpx（默认30）
	 * @property {String} sub-title 头部右边的副标题
	 * @property {String} sub-title-color 副标题颜色（默认#909399）
	 * @property {String | Number} sub-title-size 副标题字体大小（默认26）
	 * @property {Boolean} border 是否显示边框（默认true）
	 * @property {String | Number} index 用于标识点击了第几个卡片
	 * @property {String} box-shadow 卡片外围阴影，字符串形式（默认none）
	 * @property {String} margin 卡片与屏幕两边和上下元素的间距，需带单位，如"30rpx 20rpx"（默认30rpx）
	 * @property {String | Number} border-radius 卡片整体的圆角值，单位rpx（默认16）
	 * @property {Object} head-style 头部自定义样式，对象形式
	 * @property {Object} body-style 中部自定义样式，对象形式
	 * @property {Object} foot-style 底部自定义样式，对象形式
	 * @property {Boolean} head-border-bottom 是否显示头部的下边框（默认true）
	 * @property {Boolean} foot-border-top 是否显示底部的上边框（默认true）
	 * @property {Boolean} show-head 是否显示头部（默认true）
	 * @property {Boolean} show-head 是否显示尾部（默认true）
	 * @property {String} thumb 缩略图路径，如设置将显示在标题的左边，不建议使用相对路径
	 * @property {String | Number} thumb-width 缩略图的宽度，高等于宽，单位rpx（默认60）
	 * @property {Boolean} thumb-circle 缩略图是否为圆形（默认false）
	 * @event {Function} click 整个卡片任意位置被点击时触发
	 * @event {Function} head-click 卡片头部被点击时触发
	 * @event {Function} body-click 卡片主体部分被点击时触发
	 * @event {Function} foot-click 卡片底部部分被点击时触发
	 * @example \\<u-card padding="30" title="card">\\</u-card>
	 */`,
	'u-cell-group': `/**
	 * cellGroup 单元格父组件Group
	 * @description cell单元格一般用于一组列表的情况，比如个人中心页，设置页等。搭配u-cell-item
	 * @tutorial https://www.uviewui.com/components/cell.html
	 * @property {String} title 分组标题
	 * @property {Boolean} border 是否显示外边框（默认true）
	 * @property {Object} title-style 分组标题的的样式，对象形式，如{'font-size': '24rpx'} 或 {'fontSize': '24rpx'}
	 * @example \\<u-cell-group title="设置喜好">
	 */`,
	'u-cell-item': `/**
	 * cellItem 单元格Item
	 * @description cell单元格一般用于一组列表的情况，比如个人中心页，设置页等。搭配u-cell-group使用
	 * @tutorial https://www.uviewui.com/components/cell.html
	 * @property {String} title 左侧标题
	 * @property {String} icon 左侧图标名，只支持uView内置图标，见Icon 图标
	 * @property {Object} icon-style 左边图标的样式，对象形式
	 * @property {String} value 右侧内容
	 * @property {String} label 标题下方的描述信息
	 * @property {Boolean} border-bottom 是否显示cell的下边框（默认true）
	 * @property {Boolean} border-top 是否显示cell的上边框（默认false）
	 * @property {Boolean} center 是否使内容垂直居中（默认false）
	 * @property {String} hover-class 是否开启点击反馈，none为无效果（默认true）
	 * // @property {Boolean} border-gap border-bottom为true时，Cell列表中间的条目的下边框是否与左边有一个间隔（默认true）
	 * @property {Boolean} arrow 是否显示右侧箭头（默认true）
	 * @property {Boolean} required 箭头方向，可选值（默认right）
	 * @property {Boolean} arrow-direction 是否显示左边表示必填的星号（默认false）
	 * @property {Object} title-style 标题样式，对象形式
	 * @property {Object} value-style 右侧内容样式，对象形式
	 * @property {Object} label-style 标题下方描述信息的样式，对象形式
	 * @property {String} bg-color 背景颜色（默认transparent）
	 * @property {String Number} index 用于在click事件回调中返回，标识当前是第几个Item
	 * @property {String Number} title-width 标题的宽度，单位rpx
	 * @example \\<u-cell-item icon="integral-fill" title="会员等级" value="新版本">\\</u-cell-item>
	 */`,
	'u-checkbox': `/**
	 * checkbox 复选框
	 * @description 该组件需要搭配checkboxGroup组件使用，以便用户进行操作时，获得当前复选框组的选中情况。
	 * @tutorial https://www.uviewui.com/components/checkbox.html
	 * @property {String Number} icon-size 图标大小，单位rpx（默认20）
	 * @property {String Number} label-size label字体大小，单位rpx（默认28）
	 * @property {String Number} name checkbox组件的标示符
	 * @property {String} shape 形状，见官网说明（默认circle）
	 * @property {Boolean} disabled 是否禁用
	 * @property {Boolean} label-disabled 是否禁止点击文本操作checkbox
	 * @property {String} active-color 选中时的颜色，如设置CheckboxGroup的active-color将失效
	 * @event {Function} change 某个checkbox状态发生变化时触发，回调为一个对象
	 * @example \\<u-checkbox v-model="checked" :disabled="false">天涯\\</u-checkbox>
	 */`,
	'u-checkbox-group': `/**
	 * checkboxGroup 开关选择器父组件Group
	 * @description 复选框组件一般用于需要多个选择的场景，该组件功能完整，使用方便
	 * @tutorial https://www.uviewui.com/components/checkbox.html
	 * @property {String Number} max 最多能选中多少个checkbox（默认999）
	 * @property {String Number} size 组件整体的大小，单位rpx（默认40）
	 * @property {Boolean} disabled 是否禁用所有checkbox（默认false）
	 * @property {String Number} icon-size 图标大小，单位rpx（默认20）
	 * @property {Boolean} label-disabled 是否禁止点击文本操作checkbox(默认false)
	 * @property {String} width 宽度，需带单位
	 * @property {String} width 宽度，需带单位
	 * @property {String} shape 外观形状，shape-方形，circle-圆形(默认circle)
	 * @property {Boolean} wrap 是否每个checkbox都换行（默认false）
	 * @property {String} active-color 选中时的颜色，应用到所有子Checkbox组件（默认#2979ff）
	 * @event {Function} change 任一个checkbox状态发生变化时触发，回调为一个对象
	 * @example \\<u-checkbox-group>\\</u-checkbox-group>
	 */`,
	'u-circle-progress': `/**
	 * circleProgress 环形进度条
	 * @description 展示操作或任务的当前进度，比如上传文件，是一个圆形的进度条。注意：此组件的percent值只能动态增加，不能动态减少。
	 * @tutorial https://www.uviewui.com/components/circleProgress.html
	 * @property {String Number} percent 圆环进度百分比值，为数值类型，0-100
	 * @property {String} inactive-color 圆环的底色，默认为灰色(该值无法动态变更)（默认#ececec）
	 * @property {String} active-color 圆环激活部分的颜色(该值无法动态变更)（默认#19be6b）
	 * @property {String Number} width 整个圆环组件的宽度，高度默认等于宽度值，单位rpx（默认200）
	 * @property {String Number} border-width 圆环的边框宽度，单位rpx（默认14）
	 * @property {String Number} duration 整个圆环执行一圈的时间，单位ms（默认呢1500）
	 * @property {String} type 如设置，active-color值将会失效
	 * @property {String} bg-color 整个组件背景颜色，默认为白色
	 * @example \\<u-circle-progress active-color="#2979ff" :percent="80">\\</u-circle-progress>
	 */`,
	'u-col': `/**
	 * col 布局单元格
	 * @description 通过基础的 12 分栏，迅速简便地创建布局（搭配<u-row>使用）
	 * @tutorial https://www.uviewui.com/components/layout.html
	 * @property {String Number} span 栅格占据的列数，总12等分（默认0）
	 * @property {String} text-align 文字水平对齐方式（默认left）
	 * @property {String Number} offset 分栏左边偏移，计算方式与span相同（默认0）
	 * @example \\<u-col span="3">\\<view class="demo-layout bg-purple">\\</view>\\</u-col>
	 */`,
	'u-collapse': `/**
	 * collapse 手风琴
	 * @description 通过折叠面板收纳内容区域
	 * @tutorial https://www.uviewui.com/components/collapse.html
	 * @property {Boolean} accordion 是否手风琴模式（默认true）
	 * @property {Boolean} arrow 是否显示标题右侧的箭头（默认true）
	 * @property {String} arrow-color 标题右侧箭头的颜色（默认#909399）
	 * @property {Object} head-style 标题自定义样式，对象形式
	 * @property {Object} body-style 主体自定义样式，对象形式
	 * @property {String} hover-class 样式类名，按下时有效（默认u-hover-class）
	 * @event {Function} change 当前激活面板展开时触发(如果是手风琴模式，参数activeNames类型为String，否则为Array)
	 * @example \\<u-collapse>\\</u-collapse>
	 */`,
	'u-collapse-item': `/**
	 * collapseItem 手风琴Item
	 * @description 通过折叠面板收纳内容区域（搭配u-collapse使用）
	 * @tutorial https://www.uviewui.com/components/collapse.html
	 * @property {String} title 面板标题
	 * @property {String Number} index 主要用于事件的回调，标识那个Item被点击
	 * @property {Boolean} disabled 面板是否可以打开或收起（默认false）
	 * @property {Boolean} open 设置某个面板的初始状态是否打开（默认false）
	 * @property {String Number} name 唯一标识符，如不设置，默认用当前collapse-item的索引值
	 * @property {String} align 标题的对齐方式（默认left）
	 * @property {Object} active-style 不显示箭头时，可以添加当前选择的collapse-item活动样式，对象形式
	 * @event {Function} change 某个item被打开或者收起时触发
	 * @example \\<u-collapse-item :title="item.head" v-for="(item, index) in itemList" :key="index">{{item.body}}\\</u-collapse-item>
	 */`,
	'u-column-notice': ``,
	'u-count-down': `/**
	 * countDown 倒计时
	 * @description 该组件一般使用于某个活动的截止时间上，通过数字的变化，给用户明确的时间感受，提示用户进行某一个行为操作。
	 * @tutorial https://www.uviewui.com/components/countDown.html
	 * @property {String Number} timestamp 倒计时，单位为秒
	 * @property {Boolean} autoplay 是否自动开始倒计时，如果为false，需手动调用开始方法。见官网说明（默认true）
	 * @property {String} separator 分隔符，colon为英文冒号，zh为中文（默认colon）
	 * @property {String Number} separator-size 分隔符的字体大小，单位rpx（默认30）
	 * @property {String} separator-color 分隔符的颜色（默认#303133）
	 * @property {String Number} font-size 倒计时字体大小，单位rpx（默认30）
	 * @property {Boolean} show-border 是否显示倒计时数字的边框（默认false）
	 * @property {Boolean} hide-zero-day 当"天"的部分为0时，隐藏该字段 （默认true）
	 * @property {String} border-color 数字边框的颜色（默认#303133）
	 * @property {String} bg-color 倒计时数字的背景颜色（默认#ffffff）
	 * @property {String} color 倒计时数字的颜色（默认#303133）
	 * @property {String} height 数字高度值(宽度等同此值)，设置边框时看情况是否需要设置此值，单位rpx（默认auto）
	 * @property {Boolean} show-days 是否显示倒计时的"天"部分（默认true）
	 * @property {Boolean} show-hours 是否显示倒计时的"时"部分（默认true）
	 * @property {Boolean} show-minutes 是否显示倒计时的"分"部分（默认true）
	 * @property {Boolean} show-seconds 是否显示倒计时的"秒"部分（默认true）
	 * @event {Function} end 倒计时结束
	 * @event {Function} change 每秒触发一次，回调为当前剩余的倒计秒数
	 * @example \\<u-count-down ref="uCountDown" :timestamp="86400" :autoplay="false">\\</u-count-down>
	 */`,
	'u-count-to': `/**
	 * countTo 数字滚动
	 * @description 该组件一般用于需要滚动数字到某一个值的场景，目标要求是一个递增的值。
	 * @tutorial https://www.uviewui.com/components/countTo.html
	 * @property {String Number} start-val 开始值
	 * @property {String Number} end-val 结束值
	 * @property {String Number} duration 滚动过程所需的时间，单位ms（默认2000）
	 * @property {Boolean} autoplay 是否自动开始滚动（默认true）
	 * @property {String Number} decimals 要显示的小数位数，见官网说明（默认0）
	 * @property {Boolean} use-easing 滚动结束时，是否缓动结尾，见官网说明（默认true）
	 * @property {String} separator 千位分隔符，见官网说明
	 * @property {String} color 字体颜色（默认#303133）
	 * @property {String Number} font-size 字体大小，单位rpx（默认50）
	 * @property {Boolean} bold 字体是否加粗（默认false）
	 * @event {Function} end 数值滚动到目标值时触发
	 * @example \\<u-count-to ref="uCountTo" :end-val="endVal" :autoplay="autoplay">\\</u-count-to>
	 */`,
	'u-divider': `/**
	 * divider 分割线
	 * @description 区隔内容的分割线，一般用于页面底部"没有更多"的提示。
	 * @tutorial https://www.uviewui.com/components/divider.html
	 * @property {String Number} half-width 文字左或右边线条宽度，数值或百分比，数值时单位为rpx
	 * @property {String} border-color 线条颜色，优先级高于type（默认#dcdfe6）
	 * @property {String} color 文字颜色（默认#909399）
	 * @property {String Number} fontSize 字体大小，单位rpx（默认26）
	 * @property {String} bg-color 整个divider的背景颜色（默认呢#ffffff）
	 * @property {String Number} height 整个divider的高度，单位rpx（默认40）
	 * @property {String} type 将线条设置主题色（默认primary）
	 * @property {Boolean} useSlot 是否使用slot传入内容，如果不传入，中间不会有空隙（默认true）
	 * @property {String Number} margin-top 与前一个组件的距离，单位rpx（默认0）
	 * @property {String Number} margin-bottom 与后一个组件的距离，单位rpx（0）
	 * @event {Function} click divider组件被点击时触发
	 * @example \\<u-divider color="#fa3534">长河落日圆\\</u-divider>
	 */`,
	'u-dropdown': `/**
	 * dropdown 下拉菜单
	 * @description 该组件一般用于向下展开菜单，同时可切换多个选项卡的场景
	 * @tutorial http://uviewui.com/components/dropdown.html
	 * @property {String} active-color 标题和选项卡选中的颜色（默认#2979ff）
	 * @property {String} inactive-color 标题和选项卡未选中的颜色（默认#606266）
	 * @property {Boolean} close-on-click-mask 点击遮罩是否关闭菜单（默认true）
	 * @property {Boolean} close-on-click-self 点击当前激活项标题是否关闭菜单（默认true）
	 * @property {String | Number} duration 选项卡展开和收起的过渡时间，单位ms（默认300）
	 * @property {String | Number} height 标题菜单的高度，单位任意（默认80）
	 * @property {String | Number} border-radius 菜单展开内容下方的圆角值，单位任意（默认0）
	 * @property {Boolean} border-bottom 标题菜单是否显示下边框（默认false）
	 * @property {String | Number} title-size 标题的字体大小，单位任意，数值默认为rpx单位（默认28）
	 * @event {Function} open 下拉菜单被打开时触发
	 * @event {Function} close 下拉菜单被关闭时触发
	 * @example \\<u-dropdown>\\</u-dropdown>
	 */`,
	'u-dropdown-item': `/**
	 * dropdown-item 下拉菜单
	 * @description 该组件一般用于向下展开菜单，同时可切换多个选项卡的场景
	 * @tutorial http://uviewui.com/components/dropdown.html
	 * @property {String | Number} v-model 双向绑定选项卡选择值
	 * @property {String} title 菜单项标题
	 * @property {Array[Object]} options 选项数据，如果传入了默认slot，此参数无效
	 * @property {Boolean} disabled 是否禁用此选项卡（默认false）
	 * @property {String | Number} duration 选项卡展开和收起的过渡时间，单位ms（默认300）
	 * @property {String | Number} height 弹窗下拉内容的高度(内容超出将会滚动)（默认auto）
	 * @example \\<u-dropdown-item title="标题">\\</u-dropdown-item>
	 */`,
	'u-empty': `/**
	 * empty 内容为空
	 * @description 该组件用于需要加载内容，但是加载的第一页数据就为空，提示一个"没有内容"的场景， 我们精心挑选了十几个场景的图标，方便您使用。
	 * @tutorial https://www.uviewui.com/components/empty.html
	 * @property {String} color 文字颜色（默认#c0c4cc）
	 * @property {String} text 文字提示（默认“无内容”）
	 * @property {String} src 自定义图标路径，如定义，mode参数会失效
	 * @property {String Number} font-size 提示文字的大小，单位rpx（默认28）
	 * @property {String} mode 内置的图标，见官网说明（默认data）
	 * @property {String Number} img-width 图标的宽度，单位rpx（默认240）
	 * @property {String} img-height 图标的高度，单位rpx（默认auto）
	 * @property {String Number} margin-top 组件距离上一个元素之间的距离（默认0）
	 * @property {Boolean} show 是否显示组件（默认true）
	 * @event {Function} click 点击组件时触发
	 * @event {Function} close 点击关闭按钮时触发
	 * @example \\<u-empty text="所谓伊人，在水一方" mode="list">\\</u-empty>
	 */`,
	'u-field': `/**
	 * field 输入框
	 * @description 借助此组件，可以实现表单的输入， 有"text"和"textarea"类型的，此外，借助uView的picker和actionSheet组件可以快速实现上拉菜单，时间，地区选择等， 为表单解决方案的利器。
	 * @tutorial https://www.uviewui.com/components/field.html
	 * @property {String} type 输入框的类型（默认text）
	 * @property {String} icon label左边的图标，限uView的图标名称
	 * @property {Object} icon-style 左边图标的样式，对象形式
	 * @property {Boolean} right-icon 输入框右边的图标名称，限uView的图标名称（默认false）
	 * @property {Boolean} required 是否必填，左边您显示红色"*"号（默认false）
	 * @property {String} label 输入框左边的文字提示
	 * @property {Boolean} password 是否密码输入方式(用点替换文字)，type为text时有效（默认false）
	 * @property {Boolean} clearable 是否显示右侧清空内容的图标控件(输入框有内容，且获得焦点时才显示)，点击可清空输入框内容（默认true）
	 * @property {Number String} label-width label的宽度，单位rpx（默认130）
	 * @property {String} label-align label的文字对齐方式（默认left）
	 * @property {Object} field-style 自定义输入框的样式，对象形式
	 * @property {Number | String} clear-size 清除图标的大小，单位rpx（默认30）
	 * @property {String} input-align 输入框内容对齐方式（默认left）
	 * @property {Boolean} border-bottom 是否显示field的下边框（默认true）
	 * @property {Boolean} border-top 是否显示field的上边框（默认false）
	 * @property {String} icon-color 左边通过icon配置的图标的颜色（默认#606266）
	 * @property {Boolean} auto-height 是否自动增高输入区域，type为textarea时有效（默认true）
	 * @property {String Boolean} error-message 显示的错误提示内容，如果为空字符串或者false，则不显示错误信息
	 * @property {String} placeholder 输入框的提示文字
	 * @property {String} placeholder-style placeholder的样式(内联样式，字符串)，如"color: #ddd"
	 * @property {Boolean} focus 是否自动获得焦点（默认false）
	 * @property {Boolean} fixed 如果type为textarea，且在一个"position:fixed"的区域，需要指明为true（默认false）
	 * @property {Boolean} disabled 是否不可输入（默认false）
	 * @property {Number String} maxlength 最大输入长度，设置为 -1 的时候不限制最大长度（默认140）
	 * @property {String} confirm-type 设置键盘右下角按钮的文字，仅在type="text"时生效（默认done）
	 * @event {Function} input 输入框内容发生变化时触发
	 * @event {Function} focus 输入框获得焦点时触发
	 * @event {Function} blur 输入框失去焦点时触发
	 * @event {Function} confirm 点击完成按钮时触发
	 * @event {Function} right-icon-click 通过right-icon生成的图标被点击时触发
	 * @event {Function} click 输入框被点击或者通过right-icon生成的图标被点击时触发，这样设计是考虑到传递右边的图标，一般都为需要弹出"picker"等操作时的场景，点击倒三角图标，理应发出此事件，见上方说明
	 * @example \\<u-field v-model="mobile" label="手机号" required :error-message="errorMessage">\\</u-field>
	 */`,
	'u-form': `/**
	 * form 表单
	 * @description 此组件一般用于表单场景，可以配置Input输入框，Select弹出框，进行表单验证等。
	 * @tutorial http://uviewui.com/components/form.html
	 * @property {Object} model 表单数据对象
	 * @property {Boolean} border-bottom 是否显示表单域的下划线边框
	 * @property {String} label-position 表单域提示文字的位置，left-左侧，top-上方
	 * @property {String Number} label-width 提示文字的宽度，单位rpx（默认90）
	 * @property {Object} label-style lable的样式，对象形式
	 * @property {String} label-align lable的对齐方式
	 * @property {Object} rules 通过ref设置，见官网说明
	 * @property {Array} error-type 错误的提示方式，数组形式，见上方说明(默认['message'])
	 * @example \\<u-form :model="form" ref="uForm">\\</u-form>
	 */`,
	'u-form-item': `/**
	 * form-item 表单item
	 * @description 此组件一般用于表单场景，可以配置Input输入框，Select弹出框，进行表单验证等。
	 * @tutorial http://uviewui.com/components/form.html
	 * @property {String} label 左侧提示文字
	 * @property {Object} prop 表单域model对象的属性名，在使用 validate、resetFields 方法的情况下，该属性是必填的
	 * @property {Boolean} border-bottom 是否显示表单域的下划线边框
	 * @property {String} label-position 表单域提示文字的位置，left-左侧，top-上方
	 * @property {String Number} label-width 提示文字的宽度，单位rpx（默认90）
	 * @property {Object} label-style lable的样式，对象形式
	 * @property {String} label-align lable的对齐方式
	 * @property {String} right-icon 右侧自定义字体图标(限uView内置图标)或图片地址
	 * @property {String} left-icon 左侧自定义字体图标(限uView内置图标)或图片地址
	 * @property {Object} left-icon-style 左侧图标的样式，对象形式
	 * @property {Object} right-icon-style 右侧图标的样式，对象形式
	 * @property {Boolean} required 是否显示左边的"*"号，这里仅起展示作用，如需校验必填，请通过rules配置必填规则(默认false)
	 * @example \\<u-form-item label="姓名">\\<u-input v-model="form.name" />\\</u-form-item>
	 */`,
	'u-full-screen': ``,
	'u-gap': `/**
	 * gap 间隔槽
	 * @description 该组件一般用于内容块之间的用一个灰色块隔开的场景，方便用户风格统一，减少工作量
	 * @tutorial https://www.uviewui.com/components/gap.html
	 * @property {String} bg-color 背景颜色（默认#f3f4f6）
	 * @property {String Number} height 分割槽高度，单位rpx（默认30）
	 * @property {String Number} margin-top 与前一个组件的距离，单位rpx（默认0）
	 * @property {String Number} margin-bottom 与后一个组件的距离，单位rpx（0）
	 * @example \\<u-gap height="80" bg-color="#bbb">\\</u-gap>
	 */`,
	'u-grid': `/**
	 * grid 宫格布局
	 * @description 宫格组件一般用于同时展示多个同类项目的场景，可以给宫格的项目设置徽标组件(badge)，或者图标等，也可以扩展为左右滑动的轮播形式。
	 * @tutorial https://www.uviewui.com/components/grid.html
	 * @property {String Number} col 宫格的列数（默认3）
	 * @property {Boolean} border 是否显示宫格的边框（默认true）
	 * @property {Boolean} hover-class 点击宫格的时候，是否显示按下的灰色背景（默认false）
	 * @event {Function} click 点击宫格触发
	 * @example \\<u-grid :col="3" @click="click">\\</u-grid>
	 */`,
	'u-grid-item': `/**
	 * gridItem 提示
	 * @description 宫格组件一般用于同时展示多个同类项目的场景，可以给宫格的项目设置徽标组件(badge)，或者图标等，也可以扩展为左右滑动的轮播形式。搭配u-grid使用
	 * @tutorial https://www.uviewui.com/components/grid.html
	 * @property {String} bg-color 宫格的背景颜色（默认#ffffff）
	 * @property {String Number} index 点击宫格时，返回的值
	 * @property {Object} custom-style 自定义样式，对象形式
	 * @event {Function} click 点击宫格触发
	 * @example \\<u-grid-item>\\</u-grid-item>
	 */`,
	'u-icon': `/**
	 * icon 图标
	 * @description 基于字体的图标集，包含了大多数常见场景的图标。
	 * @tutorial https://www.uviewui.com/components/icon.html
	 * @property {String} name 图标名称，见示例图标集
	 * @property {String} color 图标颜色（默认inherit）
	 * @property {String | Number} size 图标字体大小，单位rpx（默认32）
	 * @property {String | Number} label-size label字体大小，单位rpx（默认28）
	 * @property {String} label 图标右侧的label文字（默认28）
	 * @property {String} label-pos label文字相对于图标的位置，只能right或bottom（默认right）
	 * @property {String} label-color label字体颜色（默认#606266）
	 * @property {Object} custom-style icon的样式，对象形式
	 * @property {String} custom-prefix 自定义字体图标库时，需要写上此值
	 * @property {String | Number} margin-left label在右侧时与图标的距离，单位rpx（默认6）
	 * @property {String | Number} margin-top label在下方时与图标的距离，单位rpx（默认6）
	 * @property {String | Number} margin-bottom label在上方时与图标的距离，单位rpx（默认6）
	 * @property {String | Number} margin-right label在左侧时与图标的距离，单位rpx（默认6）
	 * @property {String} label-pos label相对于图标的位置，只能right或bottom（默认right）
	 * @property {String} index 一个用于区分多个图标的值，点击图标时通过click事件传出
	 * @property {String} hover-class 图标按下去的样式类，用法同uni的view组件的hover-class参数，详情见官网
	 * @property {String} width 显示图片小图标时的宽度
	 * @property {String} height 显示图片小图标时的高度
	 * @property {String} top 图标在垂直方向上的定位
	 * @property {String} top 图标在垂直方向上的定位
	 * @property {String} top 图标在垂直方向上的定位
	 * @property {Boolean} show-decimal-icon 是否为DecimalIcon
	 * @property {String} inactive-color 背景颜色，可接受主题色，仅Decimal时有效
	 * @property {String | Number} percent 显示的百分比，仅Decimal时有效
	 * @event {Function} click 点击图标时触发
	 * @example \\<u-icon name="photo" color="#2979ff" size="28">\\</u-icon>
	 */`,
	'u-image': `/**
	 * Image 图片
	 * @description 此组件为uni-app的image组件的加强版，在继承了原有功能外，还支持淡入动画、加载中、加载失败提示、圆角值和形状等。
	 * @tutorial https://uviewui.com/components/image.html
	 * @property {String} src 图片地址
	 * @property {String} mode 裁剪模式，见官网说明
	 * @property {String | Number} width 宽度，单位任意，如果为数值，则为rpx单位（默认100%）
	 * @property {String | Number} height 高度，单位任意，如果为数值，则为rpx单位（默认 auto）
	 * @property {String} shape 图片形状，circle-圆形，square-方形（默认square）
	 * @property {String | Number} border-radius 圆角值，单位任意，如果为数值，则为rpx单位（默认 0）
	 * @property {Boolean} lazy-load 是否懒加载，仅微信小程序、App、百度小程序、字节跳动小程序有效（默认 true）
	 * @property {Boolean} show-menu-by-longpress 是否开启长按图片显示识别小程序码菜单，仅微信小程序有效（默认 false）
	 * @property {String} loading-icon 加载中的图标，或者小图片（默认 photo）
	 * @property {String} error-icon 加载失败的图标，或者小图片（默认 error-circle）
	 * @property {Boolean} show-loading 是否显示加载中的图标或者自定义的slot（默认 true）
	 * @property {Boolean} show-error 是否显示加载错误的图标或者自定义的slot（默认 true）
	 * @property {Boolean} fade 是否需要淡入效果（默认 true）
	 * @property {String Number} width 传入图片路径时图片的宽度
	 * @property {String Number} height 传入图片路径时图片的高度
	 * @property {Boolean} webp 只支持网络资源，只对微信小程序有效（默认 false）
	 * @property {String | Number} duration 搭配fade参数的过渡时间，单位ms（默认 500）
	 * @event {Function} click 点击图片时触发
	 * @event {Function} error 图片加载失败时触发
	 * @event {Function} load 图片加载成功时触发
	 * @example \\<u-image width="100%" height="300rpx" :src="src">\\</u-image>
	 */`,
	'u-index-anchor': `/**
	 * indexAnchor 索引列表锚点
	 * @description 通过折叠面板收纳内容区域,搭配<u-index-anchor>使用
	 * @tutorial https://www.uviewui.com/components/indexList.html#indexanchor-props
	 * @property {Boolean} use-slot 是否使用自定义内容的插槽（默认false）
	 * @property {String Number} index 索引字符，如果定义了use-slot，此参数自动失效
	 * @property {Object} custStyle 自定义样式，对象形式，如"{color: 'red'}"
	 * @event {Function} default 锚点位置显示内容，默认为索引字符
	 * @example \\<u-index-anchor :index="item" />
	 */`,
	'u-index-list': ``,
	'u-input': `/**
	 * input 输入框
	 * @description 此组件为一个输入框，默认没有边框和样式，是专门为配合表单组件u-form而设计的，利用它可以快速实现表单验证，输入内容，下拉选择等功能。
	 * @tutorial http://uviewui.com/components/input.html
	 * @property {String} type 模式选择，见官网说明
	 * @property {Boolean} clearable 是否显示右侧的清除图标(默认true)
	 * @property {} v-model 用于双向绑定输入框的值
	 * @property {String} input-align 输入框文字的对齐方式(默认left)
	 * @property {String} placeholder placeholder显示值(默认 '请输入内容')
	 * @property {Boolean} disabled 是否禁用输入框(默认false)
	 * @property {String Number} maxlength 输入框的最大可输入长度(默认140)
	 * @property {String Number} selection-start 光标起始位置，自动聚焦时有效，需与selection-end搭配使用（默认-1）
	 * @property {String Number} maxlength 光标结束位置，自动聚焦时有效，需与selection-start搭配使用（默认-1）
	 * @property {String Number} cursor-spacing 指定光标与键盘的距离，单位px(默认0)
	 * @property {String} placeholderStyle placeholder的样式，字符串形式，如"color: red;"(默认 "color: #c0c4cc;")
	 * @property {String} confirm-type 设置键盘右下角按钮的文字，仅在type为text时生效(默认done)
	 * @property {Object} custom-style 自定义输入框的样式，对象形式
	 * @property {Boolean} focus 是否自动获得焦点(默认false)
	 * @property {Boolean} fixed 如果type为textarea，且在一个"position:fixed"的区域，需要指明为true(默认false)
	 * @property {Boolean} password-icon type为password时，是否显示右侧的密码查看图标(默认true)
	 * @property {Boolean} border 是否显示边框(默认false)
	 * @property {String} border-color 输入框的边框颜色(默认#dcdfe6)
	 * @property {Boolean} auto-height 是否自动增高输入区域，type为textarea时有效(默认true)
	 * @property {String Number} height 高度，单位rpx(text类型时为70，textarea时为100)
	 * @example \\<u-input v-model="value" :type="type" :border="border" />
	 */`,
	'u-keyboard': `/**
	 * keyboard 键盘
	 * @description 此为uViw自定义的键盘面板，内含了数字键盘，车牌号键，身份证号键盘3中模式，都有可以打乱按键顺序的选项。
	 * @tutorial https://www.uviewui.com/components/keyboard.html
	 * @property {String} mode 键盘类型，见官网基本使用的说明（默认number）
	 * @property {Boolean} dot-enabled 是否显示"."按键，只在mode=number时有效（默认true）
	 * @property {Boolean} tooltip 是否显示键盘顶部工具条（默认true）
	 * @property {String} tips 工具条中间的提示文字，见上方基本使用的说明，如不需要，请传""空字符
	 * @property {Boolean} cancel-btn 是否显示工具条左边的"取消"按钮（默认true）
	 * @property {Boolean} confirm-btn 是否显示工具条右边的"完成"按钮（默认true）
	 * @property {Boolean} mask 是否显示遮罩（默认true）
	 * @property {String} confirm-text 确认按钮的文字
	 * @property {String} cancel-text 取消按钮的文字
	 * @property {Number String} z-index 弹出键盘的z-index值（默认1075）
	 * @property {Boolean} random 是否打乱键盘按键的顺序（默认false）
	 * @property {Boolean} safe-area-inset-bottom 是否开启底部安全区适配（默认false）
	 * @property {Boolean} mask-close-able 是否允许点击遮罩收起键盘（默认true）
	 * @event {Function} change 按键被点击(不包含退格键被点击)
	 * @event {Function} cancel 键盘顶部工具条左边的"取消"按钮被点击
	 * @event {Function} confirm 键盘顶部工具条右边的"完成"按钮被点击
	 * @event {Function} backspace 键盘退格键被点击
	 * @example \\<u-keyboard mode="number" v-model="show">\\</u-keyboard> 
	 */`,
	'u-lazy-load': `/**
	 * lazyLoad 懒加载
	 * @description 懒加载使用的场景为：页面有很多图片时，APP会同时加载所有的图片，导致页面卡顿，各个位置的图片出现前后不一致等.
	 * @tutorial https://www.uviewui.com/components/lazyLoad.html
	 * @property {String Number} index 用户自定义值，在事件触发时回调，用以区分是哪个图片
	 * @property {String} image 图片路径
	 * @property {String} loading-img 预加载时的占位图
	 * @property {String} error-img 图片加载出错时的占位图
	 * @property {String} threshold 触发加载时的位置，见上方说明，单位 rpx（默认300）
	 * @property {String Number} duration 图片加载成功时，淡入淡出时间，单位ms（默认）
	 * @property {String} effect 图片加载成功时，淡入淡出的css动画效果（默认ease-in-out）
	 * @property {Boolean} is-effect 图片加载成功时，是否启用淡入淡出效果（默认true）
	 * @property {String Number} border-radius 图片圆角值，单位rpx（默认0）
	 * @property {String Number} height 图片高度，注意：实际高度可能受img-mode参数影响（默认450）
	 * @property {String Number} mg-mode 图片的裁剪模式，详见image组件裁剪模式（默认widthFix）
	 * @event {Function} click 点击图片时触发
	 * @event {Function} load 图片加载成功时触发
	 * @event {Function} error 图片加载失败时触发
	 * @example \\<u-lazy-load :image="image" :loading-img="loadingImg" :error-img="errorImg">\\</u-lazy-load>
	 */`,
	'u-line': `/**
	 * line 线条
	 * @description 此组件一般用于显示一根线条，用于分隔内容块，有横向和竖向两种模式，且能设置0.5px线条，使用也很简单
	 * @tutorial https://www.uviewui.com/components/line.html
	 * @property {String} color 线条的颜色(默认#e4e7ed)
	 * @property {String} length 长度，竖向时表现为高度，横向时表现为长度，可以为百分比，带rpx单位的值等
	 * @property {String} direction 线条的方向，row-横向，col-竖向(默认row)
	 * @property {String} border-style 线条的类型，solid-实线，dashed-方形虚线，dotted-圆点虚线(默认solid)
	 * @property {Boolean} hair-line 是否显示细线条(默认true)
	 * @property {String} margin 线条与上下左右元素的间距，字符串形式，如"30rpx"
	 * @example \\<u-line color="red">\\</u-line>
	 */`,
	'u-line-progress': `/**
	 * lineProgress 线型进度条
	 * @description 展示操作或任务的当前进度，比如上传文件，是一个线形的进度条。
	 * @tutorial https://www.uviewui.com/components/lineProgress.html
	 * @property {String Number} percent 进度条百分比值，为数值类型，0-100
	 * @property {Boolean} round 进度条两端是否为半圆（默认true）
	 * @property {String} type 如设置，active-color值将会失效
	 * @property {String} active-color 进度条激活部分的颜色（默认#19be6b）
	 * @property {String} inactive-color 进度条的底色（默认#ececec）
	 * @property {Boolean} show-percent 是否在进度条内部显示当前的百分比值数值（默认true）
	 * @property {String Number} height 进度条的高度，单位rpx（默认28）
	 * @property {Boolean} striped 是否显示进度条激活部分的条纹（默认false）
	 * @property {Boolean} striped-active 条纹是否具有动态效果（默认false）
	 * @example \\<u-line-progress :percent="70" :show-percent="true">\\</u-line-progress>
	 */`,
	'u-link': `/**
	 * link 超链接
	 * @description 该组件为超链接组件，在不同平台有不同表现形式：在APP平台会通过plus环境打开内置浏览器，在小程序中把链接复制到粘贴板，同时提示信息，在H5中通过window.open打开链接。
	 * @tutorial https://www.uviewui.com/components/link.html
	 * @property {String} color 文字颜色（默认#606266）
	 * @property {String Number} font-size 字体大小，单位rpx（默认28）
	 * @property {Boolean} under-line 是否显示下划线（默认false）
	 * @property {String} href 跳转的链接，要带上http(s)
	 * @property {String} line-color 下划线颜色，默认同color参数颜色 
	 * @property {String} mp-tips 各个小程序平台把链接复制到粘贴板后的提示语（默认“链接已复制，请在浏览器打开”）
	 * @example \\<u-link href="http://www.uviewui.com">蜀道难，难于上青天\\</u-link>
	 */`,
	'u-loading': `/**
	 * loading 加载动画
	 * @description 警此组件为一个小动画，目前用在uView的loadmore加载更多和switch开关等组件的正在加载状态场景。
	 * @tutorial https://www.uviewui.com/components/loading.html
	 * @property {String} mode 模式选择，见官网说明（默认circle）
	 * @property {String} color 动画活动区域的颜色，只对 mode = flower 模式有效（默认#c7c7c7）
	 * @property {String Number} size 加载图标的大小，单位rpx（默认34）
	 * @property {Boolean} show 是否显示动画（默认true）
	 * @example \\<u-loading mode="circle">\\</u-loading>
	 */`,
	'u-loadmore': `/**
	 * loadmore 加载更多
	 * @description 此组件一般用于标识页面底部加载数据时的状态。
	 * @tutorial https://www.uviewui.com/components/loadMore.html
	 * @property {String} status 组件状态（默认loadmore）
	 * @property {String} bg-color 组件背景颜色，在页面是非白色时会用到（默认#ffffff）
	 * @property {Boolean} icon 加载中时是否显示图标（默认true）
	 * @property {String} icon-type 加载中时的图标类型（默认circle）
	 * @property {String} icon-color icon-type为circle时有效，加载中的动画图标的颜色（默认#b7b7b7）
	 * @property {Boolean} is-dot status为nomore时，内容显示为一个"●"（默认false）
	 * @property {String} color 字体颜色（默认#606266）
	 * @property {String Number} margin-top 到上一个相邻元素的距离
	 * @property {String Number} margin-bottom 到下一个相邻元素的距离
	 * @property {Object} load-text 自定义显示的文字，见上方说明示例
	 * @event {Function} loadmore status为loadmore时，点击组件会发出此事件
	 * @example \\<u-loadmore :status="status" icon-type="iconType" load-text="loadText" />
	 */`,
	'u-mask': `/**
	 * mask 遮罩
	 * @description 创建一个遮罩层，用于强调特定的页面元素，并阻止用户对遮罩下层的内容进行操作，一般用于弹窗场景
	 * @tutorial https://www.uviewui.com/components/mask.html
	 * @property {Boolean} show 是否显示遮罩（默认false）
	 * @property {String Number} z-index z-index 层级（默认1070）
	 * @property {Object} custom-style 自定义样式对象，见上方说明
	 * @property {String Number} duration 动画时长，单位毫秒（默认300）
	 * @property {Boolean} zoom 是否使用scale对遮罩进行缩放（默认true）
	 * @property {Boolean} mask-click-able 遮罩是否可点击，为false时点击不会发送click事件（默认true）
	 * @event {Function} click mask-click-able为true时，点击遮罩发送此事件
	 * @example \\<u-mask :show="show" @click="show = false">\\</u-mask>
	 */`,
	'u-message-input': `/**
	 * messageInput 验证码输入框
	 * @description 该组件一般用于验证用户短信验证码的场景，也可以结合uView的键盘组件使用
	 * @tutorial https://www.uviewui.com/components/messageInput.html
	 * @property {String Number} maxlength 输入字符个数（默认4）
	 * @property {Boolean} dot-fill 是否用圆点填充（默认false）
	 * @property {String} mode 模式选择，见上方"基本使用"说明（默认box）
	 * @property {String Number} value 预置值
	 * @property {Boolean} breathe 是否开启呼吸效果，见上方说明（默认true）
	 * @property {Boolean} focus 是否自动获取焦点（默认false）
	 * @property {Boolean} bold 字体和输入横线是否加粗（默认true）
	 * @property {String Number} font-size 字体大小，单位rpx（默认60）
	 * @property {String} active-color 当前激活输入框的样式（默认#2979ff）
	 * @property {String} inactive-color 非激活输入框的样式，文字颜色同此值（默认#606266）
	 * @property {String | Number} width 输入框宽度，单位rpx，高等于宽（默认80）
	 * @property {Boolean} disabled-keyboard 禁止点击输入框唤起系统键盘（默认false）
	 * @event {Function} change 输入内容发生改变时触发，具体见官网说明
	 * @event {Function} finish 输入字符个数达maxlength值时触发，见官网说明
	 * @example \\<u-message-input mode="bottomLine">\\</u-message-input>
	 */`,
	'u-modal': `/**
	 * modal 模态框
	 * @description 弹出模态框，常用于消息提示、消息确认、在当前页面内完成特定的交互操作
	 * @tutorial https://www.uviewui.com/components/modal.html
	 * @property {Boolean} value 是否显示模态框
	 * @property {String | Number} z-index 层级
	 * @property {String} title 模态框标题（默认"提示"）
	 * @property {String | Number} width 模态框宽度（默认600）
	 * @property {String} content 模态框内容（默认"内容"）
	 * @property {Boolean} show-title 是否显示标题（默认true）
	 * @property {Boolean} async-close 是否异步关闭，只对确定按钮有效（默认false）
	 * @property {Boolean} show-confirm-button 是否显示确认按钮（默认true）
	 * @property {Stringr | Number} negative-top modal往上偏移的值
	 * @property {Boolean} show-cancel-button 是否显示取消按钮（默认false）
	 * @property {Boolean} mask-close-able 是否允许点击遮罩关闭modal（默认false）
	 * @property {String} confirm-text 确认按钮的文字内容（默认"确认"）
	 * @property {String} cancel-text 取消按钮的文字内容（默认"取消"）
	 * @property {String} cancel-color 取消按钮的颜色（默认"#606266"）
	 * @property {String} confirm-color 确认按钮的文字内容（默认"#2979ff"）
	 * @property {String | Number} border-radius 模态框圆角值，单位rpx（默认16）
	 * @property {Object} title-style 自定义标题样式，对象形式
	 * @property {Object} content-style 自定义内容样式，对象形式
	 * @property {Object} cancel-style 自定义取消按钮样式，对象形式
	 * @property {Object} confirm-style 自定义确认按钮样式，对象形式
	 * @property {Boolean} zoom 是否开启缩放模式（默认true）
	 * @event {Function} confirm 确认按钮被点击
	 * @event {Function} cancel 取消按钮被点击
	 * @example \\<u-modal :src="title" :content="content">\\</u-modal>
	 */`,
	'u-navbar': `/**
	 * navbar 自定义导航栏
	 * @description 此组件一般用于在特殊情况下，需要自定义导航栏的时候用到，一般建议使用uniapp自带的导航栏。
	 * @tutorial https://www.uviewui.com/components/navbar.html
	 * @property {String Number} height 导航栏高度(不包括状态栏高度在内，内部自动加上)，注意这里的单位是px（默认44）
	 * @property {String} back-icon-color 左边返回图标的颜色（默认#606266）
	 * @property {String} back-icon-name 左边返回图标的名称，只能为uView自带的图标（默认arrow-left）
	 * @property {String Number} back-icon-size 左边返回图标的大小，单位rpx（默认30）
	 * @property {String} back-text 返回图标右边的辅助提示文字
	 * @property {Object} back-text-style 返回图标右边的辅助提示文字的样式，对象形式（默认{ color: '#606266' }）
	 * @property {String} title 导航栏标题，如设置为空字符，将会隐藏标题占位区域
	 * @property {String Number} title-width 导航栏标题的最大宽度，内容超出会以省略号隐藏，单位rpx（默认250）
	 * @property {String} title-color 标题的颜色（默认#606266）
	 * @property {String Number} title-size 导航栏标题字体大小，单位rpx（默认32）
	 * @property {Function} custom-back 自定义返回逻辑方法
	 * @property {String Number} z-index 固定在顶部时的z-index值（默认980）
	 * @property {Boolean} is-back 是否显示导航栏左边返回图标和辅助文字（默认true）
	 * @property {Object} background 导航栏背景设置，见官网说明（默认{ background: '#ffffff' }）
	 * @property {Boolean} is-fixed 导航栏是否固定在顶部（默认true）
	 * @property {Boolean} immersive 沉浸式，允许fixed定位后导航栏塌陷，仅fixed定位下生效（默认false）
	 * @property {Boolean} border-bottom 导航栏底部是否显示下边框，如定义了较深的背景颜色，可取消此值（默认true）
	 * @example \\<u-navbar back-text="返回" title="剑未配妥，出门已是江湖">\\</u-navbar>
	 */`,
	'u-no-network': `/**
	 * noNetwork 无网络提示
	 * @description 该组件无需任何配置，引入即可，内部自动处理所有功能和事件。
	 * @tutorial https://www.uviewui.com/components/noNetwork.html
	 * @property {String} tips 没有网络时的提示语（默认哎呀，网络信号丢失）
	 * @property {String Number} zIndex 组件的z-index值（默认1080）
	 * @property {String} image 无网络的图片提示，可用的src地址或base64图片
	 * @event {Function} retry 用户点击页面的"重试"按钮时触发
	 * @example \\<u-no-network>\\</u-no-network>
	 */`,
	'u-notice-bar': `/**
	 * noticeBar 滚动通知
	 * @description 该组件用于滚动通告场景，有多种模式可供选择
	 * @tutorial https://www.uviewui.com/components/noticeBar.html
	 * @property {Array} list 滚动内容，数组形式，见上方说明
	 * @property {String} type 显示的主题（默认warning）
	 * @property {Boolean} volume-icon 是否显示小喇叭图标（默认true）
	 * @property {Boolean} more-icon 是否显示右边的向右箭头（默认false）
	 * @property {Boolean} close-icon 是否显示关闭图标（默认false）
	 * @property {Boolean} autoplay 是否自动播放（默认true）
	 * @property {String} color 文字颜色
	 * @property {String Number} bg-color 背景颜色
	 * @property {String} mode 滚动模式（默认horizontal）
	 * @property {Boolean} show 是否显示（默认true）
	 * @property {String Number} font-size 字体大小，单位rpx（默认28）
	 * @property {String Number} volume-size 左边喇叭的大小（默认34）
	 * @property {String Number} duration 滚动周期时长，只对步进模式有效，横向衔接模式无效，单位ms（默认2000）
	 * @property {String Number} speed 水平滚动时的滚动速度，即每秒移动多少距离，只对水平衔接方式有效，单位rpx（默认160）
	 * @property {String Number} font-size 字体大小，单位rpx（默认28）
	 * @property {Boolean} is-circular mode为horizontal时，指明是否水平衔接滚动（默认true）
	 * @property {String} play-state 播放状态，play - 播放，paused - 暂停（默认play）
	 * @property {String Nubmer} border-radius 通知栏圆角（默认为0）
	 * @property {String Nubmer} padding 内边距，字符串，与普通的内边距css写法一直（默认"18rpx 24rpx"）
	 * @property {Boolean} no-list-hidden 列表为空时，是否显示组件（默认false）
	 * @property {Boolean} disable-touch 是否禁止通过手动滑动切换通知，只有mode = vertical，或者mode = horizontal且is-circular = false时有效（默认true）
	 * @event {Function} click 点击通告文字触发，只有mode = vertical，或者mode = horizontal且is-circular = false时有效
	 * @event {Function} close 点击右侧关闭图标触发
	 * @event {Function} getMore 点击右侧向右图标触发
	 * @event {Function} end 列表的消息每次被播放一个周期时触发，只有mode = vertical，或者mode = horizontal且is-circular = false时有效
	 * @example \\<u-notice-bar :more-icon="true" :list="list">\\</u-notice-bar>
	 */`,
	'u-number-box': `/**
	 * numberBox 步进器
	 * @description 该组件一般用于商城购物选择物品数量的场景。注意：该输入框只能输入大于或等于0的整数，不支持小数输入
	 * @tutorial https://www.uviewui.com/components/numberBox.html
	 * @property {Number} value 输入框初始值（默认1）
	 * @property {String} bg-color 输入框和按钮的背景颜色（默认#F2F3F5）
	 * @property {Number} min 用户可输入的最小值（默认0）
	 * @property {Number} max 用户可输入的最大值（默认99999）
	 * @property {Number} step 步长，每次加或减的值（默认1）
	 * @property {Boolean} disabled 是否禁用操作，禁用后无法加减或手动修改输入框的值（默认false）
	 * @property {Boolean} disabled-input 是否禁止输入框手动输入值（默认false）
	 * @property {Boolean} positive-integer 是否只能输入正整数（默认true）
	 * @property {String | Number} size 输入框文字和按钮字体大小，单位rpx（默认26）
	 * @property {String} color 输入框文字和加减按钮图标的颜色（默认#323233）
	 * @property {String | Number} input-width 输入框宽度，单位rpx（默认80）
	 * @property {String | Number} input-height 输入框和按钮的高度，单位rpx（默认50）
	 * @property {String | Number} index 事件回调时用以区分当前发生变化的是哪个输入框
	 * @property {Boolean} long-press 是否开启长按连续递增或递减(默认true)
	 * @property {String | Number} press-time 开启长按触发后，每触发一次需要多久，单位ms(默认250)
	 * @property {String | Number} cursor-spacing 指定光标于键盘的距离，避免键盘遮挡输入框，单位rpx（默认200）
	 * @event {Function} change 输入框内容发生变化时触发，对象形式
	 * @event {Function} blur 输入框失去焦点时触发，对象形式
	 * @event {Function} minus 点击减少按钮时触发(按钮可点击情况下)，对象形式
	 * @event {Function} plus 点击增加按钮时触发(按钮可点击情况下)，对象形式
	 * @example \\<u-number-box :min="1" :max="100">\\</u-number-box>
	 */`,
	'u-parse': `/**
	 * Parser 富文本组件
	 * @tutorial https://github.com/jin-yufeng/Parser
	 * @property {String} html 富文本数据
	 * @property {Boolean} autopause 是否在播放一个视频时自动暂停其他视频
	 * @property {Boolean} autoscroll 是否自动给所有表格添加一个滚动层
	 * @property {Boolean} autosetTitle 是否自动将 title 标签中的内容设置到页面标题
	 * @property {Number} compress 压缩等级
	 * @property {String} domain 图片、视频等链接的主域名
	 * @property {Boolean} lazyLoad 是否开启图片懒加载
	 * @property {String} loadingImg 图片加载完成前的占位图
	 * @property {Boolean} selectable 是否开启长按复制
	 * @property {Object} tagStyle 标签的默认样式
	 * @property {Boolean} showWithAnimation 是否使用渐显动画
	 * @property {Boolean} useAnchor 是否使用锚点
	 * @property {Boolean} useCache 是否缓存解析结果
	 * @event {Function} parse 解析完成事件
	 * @event {Function} load dom 加载完成事件
	 * @event {Function} ready 所有图片加载完毕事件
	 * @event {Function} error 错误事件
	 * @event {Function} imgtap 图片点击事件
	 * @event {Function} linkpress 链接点击事件
	 * @author JinYufeng
	 * @version 20201029
	 * @listens MIT
	 */`,
	'u-picker': `/**
	 * picker picker弹出选择器
	 * @description 此选择器有两种弹出模式：一是时间模式，可以配置年，日，月，时，分，秒参数 二是地区模式，可以配置省，市，区参数
	 * @tutorial https://www.uviewui.com/components/picker.html
	 * @property {Object} params 需要显示的参数，见官网说明
	 * @property {String} mode 模式选择，region-地区类型，time-时间类型（默认time）
	 * @property {String Number} start-year 可选的开始年份，mode=time时有效（默认1950）
	 * @property {String Number} end-year 可选的结束年份，mode=time时有效（默认2050）
	 * @property {Boolean} safe-area-inset-bottom 是否开启底部安全区适配（默认false）
	 * @property {Boolean} show-time-tag 时间模式时，是否显示后面的年月日中文提示
	 * @property {String} cancel-color 取消按钮的颜色（默认#606266）
	 * @property {String} confirm-color 确认按钮的颜色（默认#2979ff）
	 * @property {String} default-time 默认选中的时间，mode=time时有效
	 * @property {String} confirm-text 确认按钮的文字
	 * @property {String} cancel-text 取消按钮的文字
	 * @property {String} default-region 默认选中的地区，中文形式，mode=region时有效
	 * @property {String} default-code 默认选中的地区，编号形式，mode=region时有效
	 * @property {Boolean} mask-close-able 是否允许通过点击遮罩关闭Picker（默认true）
	 * @property {String Number} z-index 弹出时的z-index值（默认1075）
	 * @property {Array} default-selector 数组形式，其中每一项表示选择了range对应项中的第几个
	 * @property {Array} range 自定义选择的数据，mode=selector或mode=multiSelector时有效
	 * @property {String} range-key 当range参数的元素为对象时，指定Object中的哪个key的值作为选择器显示内容
	 * @event {Function} confirm 点击确定按钮，返回当前选择的值
	 * @event {Function} cancel 点击取消按钮，返回当前选择的值
	 * @example \\<u-picker v-model="show" mode="time">\\</u-picker>
	 */`,
	'u-popup': `/**
	 * popup 弹窗
	 * @description 弹出层容器，用于展示弹窗、信息提示等内容，支持上、下、左、右和中部弹出。组件只提供容器，内部内容由用户自定义
	 * @tutorial https://www.uviewui.com/components/popup.html
	 * @property {String} mode 弹出方向（默认left）
	 * @property {Boolean} mask 是否显示遮罩（默认true）
	 * @property {Stringr | Number} length mode=left | 见官网说明（默认auto）
	 * @property {Boolean} zoom 是否开启缩放动画，只在mode为center时有效（默认true）
	 * @property {Boolean} safe-area-inset-bottom 是否开启底部安全区适配（默认false）
	 * @property {Boolean} mask-close-able 点击遮罩是否可以关闭弹出层（默认true）
	 * @property {Object} custom-style 用户自定义样式
	 * @property {Stringr | Number} negative-top 中部弹出时，往上偏移的值
	 * @property {Numberr | String} border-radius 弹窗圆角值（默认0）
	 * @property {Numberr | String} z-index 弹出内容的z-index值（默认1075）
	 * @property {Boolean} closeable 是否显示关闭图标（默认false）
	 * @property {String} close-icon 关闭图标的名称，只能uView的内置图标
	 * @property {String} close-icon-pos 自定义关闭图标位置（默认top-right）
	 * @property {String} close-icon-color 关闭图标的颜色（默认#909399）
	 * @property {Number | String} close-icon-size 关闭图标的大小，单位rpx（默认30）
	 * @event {Function} open 弹出层打开
	 * @event {Function} close 弹出层收起
	 * @example \\<u-popup v-model="show">\\<view>出淤泥而不染，濯清涟而不妖\\</view>\\</u-popup>
	 */`,
	'u-radio': `/**
	 * radio 单选框
	 * @description 单选框用于有一个选择，用户只能选择其中一个的场景。搭配u-radio-group使用
	 * @tutorial https://www.uviewui.com/components/radio.html
	 * @property {String Number} icon-size 图标大小，单位rpx（默认24）
	 * @property {String Number} label-size label字体大小，单位rpx（默认28）
	 * @property {String Number} name radio组件的标示符
	 * @property {String} shape 形状，见上方说明（默认circle）
	 * @property {Boolean} disabled 是否禁用（默认false）
	 * @property {Boolean} label-disabled 点击文本是否可以操作radio（默认true）
	 * @property {String} active-color 选中时的颜色，如设置parent的active-color将失效
	 * @event {Function} change 某个radio状态发生变化时触发(选中状态)
	 * @example <u-radio :label-disabled="false">门掩黄昏，无计留春住</u-radio>
	 */`,
	'u-radio-group': `/**
	 * radioRroup 单选框父组件
	 * @description 单选框用于有一个选择，用户只能选择其中一个的场景。搭配u-radio使用
	 * @tutorial https://www.uviewui.com/components/radio.html
	 * @property {Boolean} disabled 是否禁用所有radio（默认false）
	 * @property {String Number} size 组件整体的大小，单位rpx（默认40）
	 * @property {String} active-color 选中时的颜色，应用到所有子Radio组件（默认#2979ff）
	 * @property {String Number} icon-size 图标大小，单位rpx（默认20）
	 * @property {String} shape 外观形状，shape-方形，circle-圆形(默认circle)
	 * @property {Boolean} label-disabled 是否禁止点击文本操作checkbox(默认false)
	 * @property {String} width 宽度，需带单位
	 * @property {Boolean} wrap 是否每个radio都换行（默认false）
	 * @event {Function} change 任一个radio状态发生变化时触发
	 * @example \\<u-radio-group v-model="value">\\</u-radio-group>
	 */`,
	'u-rate': `/**
	 * rate 评分
	 * @description 该组件一般用于满意度调查，星型评分的场景
	 * @tutorial https://www.uviewui.com/components/rate.html
	 * @property {String Number} count 最多可选的星星数量（默认5）
	 * @property {String Number} current 默认选中的星星数量（默认0）
	 * @property {Boolean} disabled 是否禁止用户操作（默认false）
	 * @property {String Number} size 星星的大小，单位rpx（默认32）
	 * @property {String} inactive-color 未选中星星的颜色（默认#b2b2b2）
	 * @property {String} active-color 选中的星星颜色（默认#FA3534）
	 * @property {String} active-icon 选中时的图标名，只能为uView的内置图标（默认star-fill）
	 * @property {String} inactive-icon 未选中时的图标名，只能为uView的内置图标（默认star）
	 * @property {String} gutter 星星之间的距离（默认10）
	 * @property {String Number} min-count 最少选中星星的个数（默认0）
	 * @property {Boolean} allow-half 是否允许半星选择（默认false）
	 * @event {Function} change 选中的星星发生变化时触发
	 * @example \\<u-rate :count="count" :current="2">\\</u-rate>
	 */`,
	'u-read-more': `/**
	 * readMore 阅读更多
	 * @description 该组件一般用于内容较长，预先收起一部分，点击展开全部内容的场景。
	 * @tutorial https://www.uviewui.com/components/readMore.html
	 * @property {String Number} show-height 内容超出此高度才会显示展开全文按钮，单位rpx（默认400）
	 * @property {Boolean} toggle 展开后是否显示收起按钮（默认false）
	 * @property {String} close-text 关闭时的提示文字（默认“展开阅读全文”）
	 * @property {String Number} font-size 提示文字的大小，单位rpx（默认28）
	 * @property {String} text-indent 段落首行缩进的字符个数（默认2em）
	 * @property {String} open-text 展开时的提示文字（默认“收起”）
	 * @property {String} color 提示文字的颜色（默认#2979ff）
	 * @example \\<u-read-more>\\<rich-text :nodes="content">\\</rich-text>\\</u-read-more>
	 */`,
	'u-row': `/**
	 * row 行布局
	 * @description 通过基础的 12 分栏，迅速简便地创建布局。
	 * @tutorial https://www.uviewui.com/components/layout.html#row-props
	 * @property {String Number} gutter 栅格间隔，左右各为此值的一半，单位rpx（默认0）
	 * @property {String} justify 水平排列方式(微信小程序暂不支持)默认（start(或flex-start)）
	 * @property {String} align 垂直排列方式（默认center）
	 * @example \\<u-row gutter="16">\\</u-row>
	 */`,
	'u-row-notice': ``,
	'u-search': `/**
	 * search 搜索框
	 * @description 搜索组件，集成了常见搜索框所需功能，用户可以一键引入，开箱即用。
	 * @tutorial https://www.uviewui.com/components/search.html
	 * @property {String} shape 搜索框形状，round-圆形，square-方形（默认round）
	 * @property {String} bg-color 搜索框背景颜色（默认#f2f2f2）
	 * @property {String} border-color 边框颜色，配置了颜色，才会有边框
	 * @property {String} placeholder 占位文字内容（默认“请输入关键字”）
	 * @property {Boolean} clearabled 是否启用清除控件（默认true）
	 * @property {Boolean} focus 是否自动获得焦点（默认false）
	 * @property {Boolean} show-action 是否显示右侧控件（默认true）
	 * @property {String} action-text 右侧控件文字（默认“搜索”）
	 * @property {Object} action-style 右侧控件的样式，对象形式
	 * @property {String} input-align 输入框内容水平对齐方式（默认left）
	 * @property {Object} input-style 自定义输入框样式，对象形式
	 * @property {Boolean} disabled 是否启用输入框（默认false）
	 * @property {String} search-icon-color 搜索图标的颜色，默认同输入框字体颜色
	 * @property {String} color 输入框字体颜色（默认#606266）
	 * @property {String} placeholder-color placeholder的颜色（默认#909399）
	 * @property {String} search-icon 输入框左边的图标，可以为uView图标名称或图片路径
	 * @property {String} margin 组件与其他上下左右元素之间的距离，带单位的字符串形式，如"30rpx"
	 * @property {Boolean} animation 是否开启动画，见上方说明（默认false）
	 * @property {String} value 输入框初始值
	 * @property {String | Number} maxlength 输入框最大能输入的长度，-1为不限制长度
	 * @property {Boolean} input-style input输入框的样式，可以定义文字颜色，大小等，对象形式
	 * @property {String | Number} height 输入框高度，单位rpx（默认64）
	 * @event {Function} change 输入框内容发生变化时触发
	 * @event {Function} search 用户确定搜索时触发，用户按回车键，或者手机键盘右下角的"搜索"键时触发
	 * @event {Function} custom 用户点击右侧控件时触发
	 * @event {Function} clear 用户点击清除按钮时触发
	 * @example \\<u-search placeholder="日照香炉生紫烟" v-model="keyword">\\</u-search>
	 */`,
	'u-section': `/**
	* section 查看更多
	* @description 该组件一般用于分类信息有很多，但是限于篇幅只能列出一部分，让用户通过"查看更多"获得更多信息的场景，实际效果见演示。
	* @tutorial https://www.uviewui.com/components/section.html
	* @property {String} title 左边主标题
	* @property {String} sub-title 右边副标题（默认更多）
	* @property {Boolean} right 是否显示右边的内容（默认true）
	* @property {Boolean} showLine 是否显示左边的竖条（默认true）
	* @property {Boolean} arrow 是否显示右边箭头（默认true）
	* @property {String Number} font-size 主标题的字体大小（默认28）
	* @property {Boolean} bold 主标题是否加粗（默认true）
	* @property {String} color 主标题颜色（默认#303133）
	* @event {Function} click 组件右侧的内容被点击时触发，用于跳转"更多"
	* @example \\<u-section title="今日热门" :right="false">\\</u-section>
	*/`,
	'u-select': `/**
	* select 列选择器
	* @description 此选择器用于单列，多列，多列联动的选择场景。(从1.3.0版本起，不建议使用Picker组件的单列和多列模式，Select组件是专门为列选择而构造的组件，更简单易用。)
	* @tutorial http://uviewui.com/components/select.html
	* @property {String} mode 模式选择，"single-column"-单列模式，"mutil-column"-多列模式，"mutil-column-auto"-多列联动模式
	* @property {Array} list 列数据，数组形式，见官网说明
	* @property {Boolean} v-model 布尔值变量，用于控制选择器的弹出与收起
	* @property {Boolean} safe-area-inset-bottom 是否开启底部安全区适配(默认false)
	* @property {String} cancel-color 取消按钮的颜色（默认#606266）
	* @property {String} confirm-color 确认按钮的颜色(默认#2979ff)
	* @property {String} confirm-text 确认按钮的文字
	* @property {String} cancel-text 取消按钮的文字
	* @property {String} default-value 提供的默认选中的下标，见官网说明
	* @property {Boolean} mask-close-able 是否允许通过点击遮罩关闭Picker(默认true)
	* @property {String Number} z-index 弹出时的z-index值(默认10075)
	* @property {String} value-name 自定义list数据的value属性名 1.3.6
	* @property {String} label-name 自定义list数据的label属性名 1.3.6
	* @property {String} child-name 自定义list数据的children属性名，只对多列联动模式有效 1.3.7
	* @event {Function} confirm 点击确定按钮，返回当前选择的值
	* @example \\<u-select v-model="show" :list="list">\\</u-select>
	*/`,
	'u-skeleton': `/**
	* skeleton 骨架屏
	* @description 骨架屏一般用于页面在请求远程数据尚未完成时，页面用灰色块预显示本来的页面结构，给用户更好的体验。
	* @tutorial https://www.uviewui.com/components/skeleton.html
	* @property {String} el-color 骨架块状元素的背景颜色（默认#e5e5e5）
	* @property {String} bg-color 骨架组件背景颜色（默认#ffffff）
	* @property {Boolean} animation 骨架块是否显示动画效果（默认false）
	* @property {String Number} border-radius u-skeleton-fillet类名元素，对应的骨架块的圆角大小，单位rpx（默认10）
	* @property {Boolean} loading 是否显示骨架组件，请求完成后，将此值设置为false（默认true）
	* @example \\<u-skeleton :loading="true" :animation="true">\\</u-skeleton>
	*/`,
	'u-slider': `/**
	* slider 滑块选择器
	* @tutorial https://uviewui.com/components/slider.html
	* @property {Number | String} value 滑块默认值（默认0）
	* @property {Number | String} min 最小值（默认0）
	* @property {Number | String} max 最大值（默认100）
	* @property {Number | String} step 步长（默认1）
	* @property {Number | String} blockWidth 滑块宽度，高等于宽（30）
	* @property {Number | String} height 滑块条高度，单位rpx（默认6）
	* @property {String} inactiveColor 底部条背景颜色（默认#c0c4cc）
	* @property {String} activeColor 底部选择部分的背景颜色（默认#2979ff）
	* @property {String} blockColor 滑块颜色（默认#ffffff）
	* @property {Object} blockStyle 给滑块自定义样式，对象形式
	* @property {Boolean} disabled 是否禁用滑块(默认为false)
	* @event {Function} start 滑动触发
	* @event {Function} moving 正在滑动中
	* @event {Function} end 滑动结束
	* @example \\<u-slider v-model="value" />
	*/`,
	'u-steps': `/**
	* steps 步骤条
	* @description 该组件一般用于完成一个任务要分几个步骤，标识目前处于第几步的场景。
	* @tutorial https://www.uviewui.com/components/steps.html
	* @property {String} mode 设置模式（默认dot）
	* @property {Array} list 数轴条数据，数组。具体见上方示例
	* @property {String} type type主题（默认primary）
	* @property {String} direction row-横向，column-竖向（默认row）
	* @property {Number String} current 设置当前处于第几步
	* @property {String} active-color 已完成步骤的激活颜色，如设置，type值会失效
	* @property {String} un-active-color 未激活的颜色，用于表示未完成步骤的颜色（默认#606266）
	* @example \\<u-steps :list="numList" active-color="#fa3534">\\</u-steps>
	*/`,
	'u-sticky': `/**
	* sticky 吸顶
	* @description 该组件与CSS中position: sticky属性实现的效果一致，当组件达到预设的到顶部距离时， 就会固定在指定位置，组件位置大于预设的顶部距离时，会重新按照正常的布局排列。
	* @tutorial https://www.uviewui.com/components/sticky.html
	* @property {String Number} offset-top 吸顶时与顶部的距离，单位rpx（默认0）
	* @property {String Number} index 自定义标识，用于区分是哪一个组件
	* @property {Boolean} enable 是否开启吸顶功能（默认true）
	* @property {String} bg-color 组件背景颜色（默认#ffffff）
	* @property {String Number} z-index 吸顶时的z-index值（默认970）
	* @property {String Number} h5-nav-height 导航栏高度，自定义导航栏时(无导航栏时需设置为0)，需要传入此值，单位px（默认44）
	* @event {Function} fixed 组件吸顶时触发
	* @event {Function} unfixed 组件取消吸顶时触发
	* @example \\<u-sticky offset-top="200">\\<view>塞下秋来风景异，衡阳雁去无留意\\</view>\\</u-sticky>
	*/`,
	'u-subsection': `/**
	* subsection 分段器
	* @description 该分段器一般用于用户从几个选项中选择某一个的场景
	* @tutorial https://www.uviewui.com/components/subsection.html
	* @property {Array} list 选项的数组，形式见上方"基本使用"
	* @property {String Number} current 初始化时默认选中的选项索引值（默认0）
	* @property {String} active-color 激活时的颜色，mode为subsection时固定为白色（默认#303133）
	* @property {String} inactive-color 未激活时字体的颜色，mode为subsection时无效（默认#606266）
	* @property {String} mode 模式选择，见官网"模式选择"说明（默认button）
	* @property {String Number} font-size 字体大小，单位rpx（默认28）
	* @property {String Number} height 组件高度，单位rpx（默认70）
	* @property {Boolean} animation 是否开启动画效果，见上方说明（默认true）
	* @property {Boolean} bold 激活选项的字体是否加粗（默认true）
	* @property {String} bg-color 组件背景颜色，mode为button时有效（默认#eeeeef）
	* @property {String} button-color 按钮背景颜色，mode为button时有效（默认#ffffff）
	* @event {Function} change 分段器选项发生改变时触发
	* @example \\<u-subsection active-color="#ff9900">\\</u-subsection>
	*/`,
	'u-swipe-action': `/**
	* swipeAction 左滑单元格
	* @description 该组件一般用于左滑唤出操作菜单的场景，用的最多的是左滑删除操作。
	* @tutorial https://www.uviewui.com/components/swipeAction.html
	* @property {String} bg-color 整个组件背景颜色（默认#ffffff）
	* @property {Array} options 数组形式，可以配置背景颜色和文字
	* @property {String Number} index 标识符，点击时候用于区分点击了哪一个，用v-for循环时的index即可
	* @property {String Number} btn-width 按钮宽度，单位rpx（默认180）
	* @property {Boolean} disabled 是否禁止某个swipeAction滑动（默认false）
	* @property {Boolean} show 打开或者关闭某个组件（默认false）
	* @event {Function} click 点击组件时触发
	* @event {Function} close 组件触发关闭状态时
	* @event {Function} content-click 点击内容时触发
	* @event {Function} open 组件触发打开状态时
	* @example \\<u-swipe-action btn-text="收藏">...\\</u-swipe-action>
	*/`,
	'u-swiper': `/**
	* swiper 轮播图
	* @description 该组件一般用于导航轮播，广告展示等场景,可开箱即用
	* @tutorial https://www.uviewui.com/components/swiper.html
	* @property {Array} list 轮播图数据，见官网"基本使用"说明
	* @property {Boolean} title 是否显示标题文字，需要配合list参数，见官网说明（默认false）
	* @property {String} mode 指示器模式，见官网说明（默认round）
	* @property {String Number} height 轮播图组件高度，单位rpx（默认250）
	* @property {String} indicator-pos 指示器的位置（默认bottomCenter）
	* @property {Boolean} effect3d 是否开启3D效果（默认false）
	* @property {Boolean} autoplay 是否自动播放（默认true）
	* @property {String Number} interval 自动轮播时间间隔，单位ms（默认2500）
	* @property {Boolean} circular 是否衔接播放，见官网说明（默认true）
	* @property {String} bg-color 背景颜色（默认#f3f4f6）
	* @property {String Number} border-radius 轮播图圆角值，单位rpx（默认8）
	* @property {Object} title-style 自定义标题样式
	* @property {String Number} effect3d-previous-margin mode = true模式的情况下，激活项与前后项之间的距离，单位rpx（默认50）
	* @property {String} img-mode 图片的裁剪模式，详见image组件裁剪模式（默认aspectFill）
	* @event {Function} click 点击轮播图时触发
	* @example \\<u-swiper :list="list" mode="dot" indicator-pos="bottomRight">\\</u-swiper>
	*/`,
	'u-tabbar': ``,
	'u-table': `/**
	* table 表格
	* @description 表格组件一般用于展示大量结构化数据的场景
	* @tutorial https://www.uviewui.com/components/table.html
	* @property {String} border-color 表格边框的颜色（默认#e4e7ed）
	* @property {String} bg-color 表格的背景颜色（默认#ffffff）
	* @property {String} align 单元格的内容对齐方式，作用类似css的text-align（默认center）
	* @property {String} padding 单元格的内边距，同css的padding写法（默认10rpx 0）
	* @property {String Number} font-size 单元格字体大小，单位rpx（默认28）
	* @property {String} color 单元格字体颜色（默认#606266）
	* @property {Object} th-style th单元格的样式，对象形式(将th所需参数放在table组件，是为了避免每一个th组件要写一遍）
	* @event {Function} click 点击组件时触发
	* @event {Function} close 点击关闭按钮时触发
	* @example \\<u-table>\\</u-table>
	*/`,
	'u-tabs': `/**
	* tabs 标签
	* @description 该组件，是一个tabs标签组件，在标签多的时候，可以配置为左右滑动，标签少的时候，可以禁止滑动。 该组件的一个特点是配置为滚动模式时，激活的tab会自动移动到组件的中间位置。
	* @tutorial https://www.uviewui.com/components/tabs.html
	* @property {Boolean} is-scroll tabs是否可以左右拖动（默认true）
	* @property {Array} list 标签数组，元素为对象，如[{name: '推荐'}]
	* @property {String Number} current 指定哪个tab为激活状态（默认0）
	* @property {String Number} height 导航栏的高度，单位rpx（默认80）
	* @property {String Number} font-size tab文字大小，单位rpx（默认30）
	* @property {String Number} duration 滑块移动一次所需的时间，单位秒（默认0.5）
	* @property {String} active-color 滑块和激活tab文字的颜色（默认#2979ff）
	* @property {String} inactive-color tabs文字颜色（默认#303133）
	* @property {String Number} bar-width 滑块宽度，单位rpx（默认40）
	* @property {Object} active-item-style 活动tabs item的样式，对象形式
	* @property {Object} bar-style 底部滑块的样式，对象形式
	* @property {Boolean} show-bar 是否显示底部的滑块（默认true）
	* @property {String Number} bar-height 滑块高度，单位rpx（默认6）
	* @property {String Number} item-width 标签的宽度（默认auto）
	* @property {String Number} gutter 单个tab标签的左右内边距之和，单位rpx（默认40）
	* @property {String} bg-color tabs导航栏的背景颜色（默认#ffffff）
	* @property {String} name 组件内部读取的list参数中的属性名（tab名称），见官网说明（默认name）
	* @property {String} count 组件内部读取的list参数中的属性名（badge徽标数），同name属性的使用，见官网说明（默认count）
	* @property {Array} offset 设置badge徽标数的位置偏移，格式为 [x, y]，也即设置的为top和right的值，单位rpx（默认[5, 20]）
	* @property {Boolean} bold 激活选项的字体是否加粗（默认true）
	* @event {Function} change 点击标签时触发
	* @example \\<u-tabs ref="tabs" :list="list" :is-scroll="false">\\</u-tabs>
	*/`,
	'u-tabs-swiper': `/**
	* tabsSwiper 全屏选项卡
	* @description 该组件内部实现主要依托于uniapp的scroll-view和swiper组件，主要特色是切换过程中，tabsSwiper文字的颜色可以渐变，底部滑块可以 跟随式滑动，活动tab滚动居中等。应用场景可以用于需要左右切换页面，比如商城的订单中心(待收货-待付款-待评价-已退货)等应用场景。
	* @tutorial https://www.uviewui.com/components/tabsSwiper.html
	* @property {Boolean} is-scroll tabs是否可以左右拖动（默认true）
	* @property {Array} list 标签数组，元素为对象，如[{name: '推荐'}]
	* @property {String Number} current 指定哪个tab为激活状态（默认0）
	* @property {String Number} height 导航栏的高度，单位rpx（默认80）
	* @property {String Number} font-size tab文字大小，单位rpx（默认30）
	* @property {String Number} swiper-width tabs组件外部swiper的宽度，默认为屏幕宽度，单位rpx（默认750）
	* @property {String} active-color 滑块和激活tab文字的颜色（默认#2979ff）
	* @property {String} inactive-color tabs文字颜色（默认#303133）
	* @property {String Number} bar-width 滑块宽度，单位rpx（默认40）
	* @property {String Number} bar-height 滑块高度，单位rpx（默认6）
	* @property {Object} bar-style 底部滑块的样式，对象形式
	* @property {Object} active-item-style 活动tabs item的样式，对象形式
	* @property {Boolean} show-bar 是否显示底部的滑块（默认true）
	* @property {String Number} gutter 单个tab标签的左右内边距之和，单位rpx（默认40）
	* @property {String} bg-color tabs导航栏的背景颜色（默认#ffffff）
	* @property {String} name 组件内部读取的list参数中的属性名，见官网说明（默认name）
	* @property {String} count 组件内部读取的list参数中的属性名（badge徽标数），同name属性的使用，见官网说明（默认count）
	* @property {Array} offset 设置badge徽标数的位置偏移，格式为 [x, y]，也即设置的为top和right的值，单位rpx（默认[5, 20]）
	* @property {Boolean} bold 激活选项的字体是否加粗（默认true）
	* @event {Function} change 点击标签时触发
	* @example \\<u-tabs-swiper ref="tabs" :list="list" :is-scroll="false">\\</u-tabs-swiper>
	*/`,
	'u-tag': `/**
	* tag 提示
	* @description 该组件一般用于标记和选择
	* @tutorial https://www.uviewui.com/components/tag.html
	* @property {String} type 主题类型（默认primary）
	* @property {String} size 标签大小（默认default）
	* @property {String} shape 标签形状（默认square）
	* @property {String} text 标签的文字内容
	* @property {String} bg-color 自定义标签的背景颜色
	* @property {String} border-color 标签的边框颜色
	* @property {String} close-color 关闭按钮的颜色
	* @property {String Number} index 点击标签时，会通过click事件返回该值
	* @property {String} mode 模式选择，见官网说明（默认light）
	* @property {Boolean} closeable 是否可关闭，设置为true，文字右边会出现一个关闭图标（默认false）
	* @property {Boolean} show 标签显示与否（默认true）
	* @event {Function} click 点击标签触发
	* @event {Function} close closeable为true时，点击标签关闭按钮触发
	* @example \\<u-tag text="雪月夜" type="success" />
	*/`,
	'u-td': `/**
	* td td单元格
	* @description 表格组件一般用于展示大量结构化数据的场景（搭配u-table使用）
	* @tutorial https://www.uviewui.com/components/table.html#td-props
	* @property {String Number} width 单元格宽度百分比或者具体带单位的值，如30%， 200rpx等，一般使用百分比，单元格宽度默认为均分tr的长度（默认auto）
	* @example \\<u-td>二年级\\</u-td>
	*/`,
	'u-th': `/**
	* th th单元格
	* @description 表格组件一般用于展示大量结构化数据的场景（搭配u-table使用）
	* @tutorial https://www.uviewui.com/components/table.html#td-props
	* @property {String Number} width 标题单元格宽度百分比或者具体带单位的值，如30%，200rpx等，一般使用百分比，单元格宽度默认为均分tr的长度
	* @example 暂无示例
	*/`,
	'u-time-line': `/**
	* timeLine 时间轴
	* @description 时间轴组件一般用于物流信息展示，各种跟时间相关的记录等场景。
	* @tutorial https://www.uviewui.com/components/timeLine.html
	* @example \\<u-time-line>\\</u-time-line>
	*/`,
	'u-time-line-item': `/**
	* timeLineItem 时间轴Item
	* @description 时间轴组件一般用于物流信息展示，各种跟时间相关的记录等场景。(搭配u-time-line使用)
	* @tutorial https://www.uviewui.com/components/timeLine.html
	* @property {String} bg-color 左边节点的背景颜色，一般通过slot内容自定义背景颜色即可（默认#ffffff）
	* @property {String Number} node-top 节点左边图标绝对定位的top值，单位rpx
	* @example \\<u-time-line-item node-top="2">...\\</u-time-line-item>
	*/`,
	'u-toast': `/**
	* toast 消息提示
	* @description 此组件表现形式类似uni的uni.showToastAPI，但也有不同的地方。
	* @tutorial https://www.uviewui.com/components/toast.html
	* @property {String} z-index toast展示时的z-index值
	* @event {Function} show 显示toast，如需一进入页面就显示toast，请在onReady生命周期调用
	* @example \\<u-toast ref="uToast" />
	*/`,
	'u-top-tips': `/**
	* topTips 顶部提示
	* @description 该组件一般用于页面顶部向下滑出一个提示，尔后自动收起的场景。
	* @tutorial https://www.uviewui.com/components/topTips.html
	* @property {String Number} navbar-height 导航栏高度(包含状态栏高度在内)，单位PX
	* @property {String Number} z-index z-index值（默认975）
	* @example \\<u-top-tips ref="uTips">\\</u-top-tips>
	*/`,
	'u-tr': `/**
	* tr 表格行标签
	* @description 表格组件一般用于展示大量结构化数据的场景（搭配<u-table>使用）
	* @tutorial https://www.uviewui.com/components/table.html
	* @example <u-tr></u-tr>
	*/`,
	'u-upload': `/**
	* upload 图片上传
	* @description 该组件用于上传图片场景
	* @tutorial https://www.uviewui.com/components/upload.html
	* @property {String} action 服务器上传地址
	* @property {String Number} max-count 最大选择图片的数量（默认99）
	* @property {Boolean} custom-btn 如果需要自定义选择图片的按钮，设置为true（默认false）
	* @property {Boolean} show-progress 是否显示进度条（默认true）
	* @property {Boolean} disabled 是否启用(显示/移仓)组件（默认false）
	* @property {String} image-mode 预览图片等显示模式，可选值为uni的image的mode属性值（默认aspectFill）
	* @property {String} del-icon 右上角删除图标名称，只能为uView内置图标
	* @property {String} del-bg-color 右上角关闭按钮的背景颜色
	* @property {String | Number} index 在各个回调事件中的最后一个参数返回，用于区别是哪一个组件的事件
	* @property {String} del-color 右上角关闭按钮图标的颜色
	* @property {Object} header 上传携带的头信息，对象形式
	* @property {Object} form-data 上传额外携带的参数
	* @property {String} name 上传文件的字段名，供后端获取使用（默认file）
	* @property {Array<String>} size-type original 原图，compressed 压缩图，默认二者都有（默认['original', 'compressed']）
	* @property {Array<String>} source-type 选择图片的来源，album-从相册选图，camera-使用相机，默认二者都有（默认['album', 'camera']）
	* @property {Boolean} preview-full-image	是否可以通过uni.previewImage预览已选择的图片（默认true）
	* @property {Boolean} multiple	是否开启图片多选，部分安卓机型不支持（默认true）
	* @property {Boolean} deletable 是否显示删除图片的按钮（默认true）
	* @property {String Number} max-size 选择单个文件的最大大小，单位B(byte)，默认不限制（默认Number.MAX_VALUE）
	* @property {Array<Object>} file-list 默认显示的图片列表，数组元素为对象，必须提供url属性
	* @property {Boolean} upload-text 选择图片按钮的提示文字（默认“选择图片”）
	* @property {Boolean} auto-upload 选择完图片是否自动上传，见上方说明（默认true）
	* @property {Boolean} show-tips 特殊情况下是否自动提示toast，见上方说明（默认true）
	* @property {Boolean} show-upload-list 是否显示组件内部的图片预览（默认true）
	* @event {Function} on-oversize 图片大小超出最大允许大小
	* @event {Function} on-preview 全屏预览图片时触发
	* @event {Function} on-remove 移除图片时触发
	* @event {Function} on-success 图片上传成功时触发
	* @event {Function} on-change 图片上传后，无论成功或者失败都会触发
	* @event {Function} on-error 图片上传失败时触发
	* @event {Function} on-progress 图片上传过程中的进度变化过程触发
	* @event {Function} on-uploaded 所有图片上传完毕触发
	* @event {Function} on-choose-complete 每次选择图片后触发，只是让外部可以得知每次选择后，内部的文件列表
	* @example \\<u-upload :action="action" :file-list="fileList" >\\</u-upload>
	*/`,
	'u-verification-code': `/**
	* verificationCode 验证码输入框
	* @description 考虑到用户实际发送验证码的场景，可能是一个按钮，也可能是一段文字，提示语各有不同，所以本组件 不提供界面显示，只提供提示语，由用户将提示语嵌入到具体的场景
	* @tutorial https://www.uviewui.com/components/verificationCode.html
	* @property {Number String} seconds 倒计时所需的秒数（默认60）
	* @property {String} start-text 开始前的提示语，见官网说明（默认获取验证码）
	* @property {String} change-text 倒计时期间的提示语，必须带有字母"x"，见官网说明（默认X秒重新获取）
	* @property {String} end-text 倒计结束的提示语，见官网说明（默认重新获取）
	* @property {Boolean} keep-running 是否在H5刷新或各端返回再进入时继续倒计时（默认false）
	* @event {Function} change 倒计时期间，每秒触发一次
	* @event {Function} start 开始倒计时触发
	* @event {Function} end 结束倒计时触发
	* @example \\<u-verification-code :seconds="seconds" @end="end" @start="start" ref="uCode" 
	*/`,
	'u-waterfall': `/**
	* waterfall 瀑布流
	* @description 这是一个瀑布流形式的组件，内容分为左右两列，结合uView的懒加载组件效果更佳。相较于某些只是奇偶数左右分别，或者没有利用vue作用域插槽的做法，uView的瀑布流实现了真正的 组件化，搭配LazyLoad 懒加载和loadMore 加载更多组件，让您开箱即用，眼前一亮。
	* @tutorial https://www.uviewui.com/components/waterfall.html
	* @property {Array} flow-list 用于渲染的数据
	* @property {String Number} add-time 单条数据添加到队列的时间间隔，单位ms，见上方注意事项说明（默认200）
	* @example \\<u-waterfall :flowList="flowList">\\</u-waterfall>
	*/`,
};

export const docs: Record<string, ReturnType<typeof explanatoryNoteToDesc>> = {};

for (const [key, value] of Object.entries(_docs)) {
	docs[key] = explanatoryNoteToDesc(value);
}
