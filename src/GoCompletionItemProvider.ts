/*
 * @Author: hongbin
 * @Date: 2023-09-15 22:25:13
 * @LastEditors: hongbin
 * @LastEditTime: 2023-09-18 09:18:42
 * @Description:自动补全
 */
import * as vscode from 'vscode';
import { docs as DOCS } from './docs';

// const reg = new RegExp(/(.+)<(u-\w+) ([\w|\-]+)/);
// const reg = new RegExp(/\s+<(u-\w+)([\s|\S]*)*\s+(\w+)$/);
// const reg = new RegExp(/\s+<(u-\w+)([\s|\S]*)*\s+([\w|\:\|@]+)$/);
const reg = new RegExp(/\s+<(u-\w+)(([\s|\S]*)*\s+([\w|\:\|@]+)$)*/);
const tags = Object.keys(DOCS).map(
	(tag) => new vscode.CompletionItem(tag, vscode.CompletionItemKind.Snippet)
);

export class GoCompletionItemProvider implements vscode.CompletionItemProvider {
	public provideCompletionItems(
		document: vscode.TextDocument,
		position: vscode.Position,
		token: vscode.CancellationToken
	) {
		const line = document.lineAt(position);

		const lineText = line.text.substring(0, position.character);
		const res = lineText.match(reg);
		// console.log(lineText, res);
		// 属性方法提示
		if (res && res[1] && res[4]) {
			const docs = DOCS[res[1]];
			if (docs) {
				switch (res[4]) {
					//属性
					case ':': {
						return Object.keys(docs.properties)
							.filter((prop) => prop.includes(res[4].substring(1)))
							.map((prop) => docs.properties[prop].item);
					}
					// 方法
					case '@': {
						return Object.keys(docs.method)
							.filter((prop) => prop.includes(res[4].substring(1)))
							.map((prop) => docs.method[prop].item);
					}
					// 全部
					default: {
						return Object.keys(docs.propertiesAndMethod)
							.filter((prop) => prop.includes(res[4]))
							.map((prop) => docs.propertiesAndMethod[prop].item);
					}
				}
			}
		} else if (lineText.includes('<')) {
			//返回UView的所有标签
			return tags;
		}
	}
}
