import * as vscode from 'vscode';
import { provideDefinition } from './jumpComponent';
import { provideHover } from './hover';
import { GoCompletionItemProvider } from './GoCompletionItemProvider';

export function activate(context: vscode.ExtensionContext) {
	console.log('启动！UView');

	const jumpDisposable = vscode.languages.registerDefinitionProvider('vue', {
		provideDefinition,
	});

	const hoverDisposable = vscode.languages.registerHoverProvider('vue', {
		provideHover,
	});

	const completionDisposable = vscode.languages.registerCompletionItemProvider(
		'vue',
		new GoCompletionItemProvider(),
		'<',
		'<u',
		'<u-',
		'@',
		':'
	);

	context.subscriptions.push(jumpDisposable);
	context.subscriptions.push(hoverDisposable);
	context.subscriptions.push(completionDisposable);
}
