/*
 * @Author: hongbin
 * @Date: 2023-09-15 19:44:08
 * @LastEditors: hongbin
 * @LastEditTime: 2023-09-15 21:22:01
 * @Description:
 */
import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';

const showError = function (info: string) {
	vscode.window.showErrorMessage(info);
};

/**
 * 获取当前所在工程根目录，有3种使用方法：<br>
 * getProjectPath(uri) uri 表示工程内某个文件的路径<br>
 * getProjectPath(document) document 表示当前被打开的文件document对象<br>
 * @param {*} document
 */
export function getProjectPath(document: vscode.TextDocument) {
	if (!document) {
		showError('当前激活的编辑器不是文件或者没有文件被打开！');
		return '';
	}

	const { workspaceFolders } = vscode.workspace;
	if (!workspaceFolders) return '';

	if (workspaceFolders.length == 1) {
		return workspaceFolders[0].uri.path;
	}
	showError('获取工程根路径异常！');
}
