/*
 * @Author: hongbin
 * @Date: 2023-09-15 10:54:43
 * @LastEditors: hongbin
 * @LastEditTime: 2023-09-15 21:23:22
 * @Description:ctrl 跳转组件
 */
import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';
import { docs as DOCS } from './docs';
import { getProjectPath } from './utils';

export function provideDefinition(
	document: vscode.TextDocument,
	position: vscode.Position,
	token: vscode.CancellationToken
): vscode.ProviderResult<vscode.Definition | vscode.LocationLink[]> {
	// ctrl选中的单词
	const word = document.getText(document.getWordRangeAtPosition(position));
	// 查询有无文档
	const docs = DOCS[word];

	if (docs) {
		// 找到绝对路径
		const projectPath = getProjectPath(document);
		const destPath = `${projectPath}/src/uview-ui/components/${word}/${word}.vue`;
		// 若存在该路径 则返回Location点击时进入该路径文件
		if (fs.existsSync(destPath)) {
			return new vscode.Location(vscode.Uri.file(destPath), new vscode.Position(0, 0));
		} else {
			vscode.window.showInformationMessage(`未找到${destPath}`);
		}
	}
}
