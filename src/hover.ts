/*
 * @Author: hongbin
 * @Date: 2023-09-15 21:24:29
 * @LastEditors: hongbin
 * @LastEditTime: 2023-09-17 21:07:06
 * @Description:鼠标悬浮时提示
 */
import * as vscode from 'vscode';
import { docs as DOCS } from './docs';

const reg = new RegExp(/\s+<(u-\w+)(([\s|\S]*)*\s+([\w|\:\|@]+)$)*/);

export function provideHover(document: vscode.TextDocument, position: vscode.Position) {
	// ctrl选中的单词
	const word = document.getText(document.getWordRangeAtPosition(position));
	// 查询有无文档
	const docs = DOCS[word];

	if (docs) {
		// const range = new Range();
		return new vscode.Hover(docs.desc);
	}
	//  不是标签 属性 方法
	const lineText = document.lineAt(position).text.substring(0, position.character);
	const getTag = lineText.match(reg);
	// console.log('HOVER:', word, lineText, getTag);
	if (getTag && getTag[1] && DOCS[getTag[1]]) {
		const prop = DOCS[getTag[1]].propertiesAndMethod[word];
		if (prop.desc) return new vscode.Hover(prop.desc);
	}
}
